import http from "../http-common";

class MaterialDataService {
    getAll() {
        return http.get("/materiales");
    }

    // get(id) {
    //   return http.get(`/materiales/${id}`);
    // }

    create(data) {
        return http.post("/materiales/add", data);
    }

    update(data) {
        return http.post(`/materiales/update/`, data);
    }

    // updateEstatus(id, data) {
    //   return http.put(`/materialesBaja/${id}`, data);
    // }

    delete(data) {
        return http.post(`/materiales/delete/`, data);
    }

    //   deleteAll() {
    //     return http.delete(`/tutorials`);
    //   }

    //   findByTitle(title) {
    //     return http.get(`/tutorials?title=${title}`);
    //   }
}

export default new MaterialDataService();