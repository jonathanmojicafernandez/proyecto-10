import http from "../http-common";

class usuarioDataService {
    
    getAll() {
        return http.get(`/usuarios`);
    }
    searchUser(data){
        return http.post("/usuariosU", data);
    }
    getNoFilter() {
        return http.get(`/usuariosNoFilter`);
    }
    create(data) {
        return http.post("/usuarios/add", data);
    }
    update(data){
        return http.post(`/usuarios/update/`, data);
    }
    accepted(data){
        return http.post(`/usuarios/accepted/`, data);
    }
    rejected(data){
        return http.post(`/usuarios/rejected/`, data);
    }

}

export default new usuarioDataService();