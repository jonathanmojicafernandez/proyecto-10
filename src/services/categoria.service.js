import http from "../http-common";

class CategoriaDataService {
  getAll() {
    return http.get("/categorias");
  }

  get(id) {
    return http.get(`/categorias/${id}`);
  }

  create(data) {
    return http.post("/categorias/add", data);
  }

  update(id, data) {
    return http.post(`/categorias/update/${id}`, data);
  }

  updateEstatus(id, data) {
    return http.put(`/categoriasBaja/${id}`, data);
  }

  delete(id) {
    return http.post(`/categorias/delete/${id}`);
  }

//   deleteAll() {
//     return http.delete(`/tutorials`);
//   }

//   findByTitle(title) {
//     return http.get(`/tutorials?title=${title}`);
//   }
}

export default new CategoriaDataService();