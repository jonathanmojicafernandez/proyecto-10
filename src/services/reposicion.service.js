import http from "../http-common";

class ReposicionDataService {
    getAll() {
        return http.get("/prestamos/reposicion");
    }  
  

}

export default new ReposicionDataService();