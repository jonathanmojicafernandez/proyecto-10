import http from "../http-common";

class EntradaDataService {
    getAll() {
        return http.get("/entrada");
    }  
    create(data) {
        return http.post("/entrada/add", data);
    }
    update(data){
        return http.post(`/entrada/update/`, data);
    }
    delete(data){
        return http.post(`/entrada/delete/`, data);
    }
    search(data){
        return http.post(`/entrada/search`, data);
    }
    getClave(data){
        return http.get(`/entrada/getKeys/${data}`);
    }
    changeStatus(data){
        return http.post(`/entrada/changeStatus`, data);
    }

}

export default new EntradaDataService();