import http from "../http-common";

class AdeudoDataService {
    getAll() {
        return http.get("/prestamos/adeudos");
    }  
  

}

export default new AdeudoDataService();