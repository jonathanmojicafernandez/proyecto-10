import http from "../http-common";

class rangosDataService {
    getAll() {
        return http.get(`/rangos`);
    }
}
export default new rangosDataService();