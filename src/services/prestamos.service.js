import http from "../http-common";

class PrestamoDataService {
    getAll() {
        return http.get("/prestamos");
    }  
    create(data) {
        return http.post("/prestamos/add", data);
    }
    update(data){
        return http.post(`/prestamos/update/`, data);
    }
    devolucion(data){
        return http.post(`/prestamos/devolucion/`, data);
    }
    getClave(data){
        return http.get(`/prestamos/getKeys/${data}`);
    }
    changeToReposicioin(data){
        return http.post("/prestamos/changeReposicion", data);
    }

}

export default new PrestamoDataService();