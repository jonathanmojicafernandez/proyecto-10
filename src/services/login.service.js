import http from "../http-common-login";

class loginDataService {

    login(data) {
        return http.post(`/login`, data);
      }

}

export default new loginDataService();