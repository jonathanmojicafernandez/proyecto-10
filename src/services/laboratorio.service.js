import http from "../http-common";

class laboratorioDataService {
    getAll() {
        return http.get("/laboratorios");
    }  
    create(data) {
        return http.post("/laboratorios/add", data);
    }
    update(data){
        return http.post(`/laboratorios/update/`, data);
    }
    delete(data){
        return http.post(`/laboratorios/delete/`, data);
    }

}

export default new laboratorioDataService();