import React, { Component, useState } from 'react'
import "../estilos/categoria.css";
import Input from '../input/Input';
import CategoriaDataService from "../../services/categoria.service";
import Swal from "sweetalert2";

const ModalAgregar = ({ setagregado }) => {

    const [nombre, setnombre] = useState("")

    const onChangeNombre = (e) => {
        setnombre(e);
    }

    const saveCategoria = async () => {
        if (nombre.length > 0) {
            let data = {
                nombre: nombre
            };
             CategoriaDataService.create(data).then();
                Swal.fire({
                    position: "center",
                    icon: "success",
                    title: "Se registrado correctamente la categoria",
                    showConfirmButton: false,
                    timer: 1500,
                });
                setagregado(true)
        } else {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No ha ingresado ninguna categoria",
                showConfirmButton: false,
                timer: 1500,
            });
        }

    };

    return (

        <div>
            <div className="modal fade" id="staticBackdrop1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">Agregar categoría</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-sm-12 col-md-12 col-lg-12">
                                    <input id="nombre"
                                        className="form-control rounded-item"
                                        onChange={(e) => {
                                            onChangeNombre(e.target.value);
                                        }}
                                        name="nombre" label="Nombre Categoría" type="text" />
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button onClick={saveCategoria} className="btn btn-success" data-bs-dismiss="modal" id="btnModificar">Agregar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModalAgregar

