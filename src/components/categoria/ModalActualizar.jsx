import React, { useState, useEffect, useCallback } from 'react'
import "../estilos/categoria.css";
import Swal from 'sweetalert2';
import PropTypes from 'prop-types'
import CategoriaDataService from "../../services/categoria.service";

const ModalActualizar = props => {
    const [nombre, setNombre] = useState(props.categoria.nombre);
    const [block, setBlock] = useState(false)
    const [estatus, setEstatus] = useState(props.categoria.estatus);

    const changeName = (e) => {
        setNombre(e.target.value);
        console.log(nombre);
    };

    const changeEstatus = () => {
        if (estatus == "Activo") {
            setEstatus("Inactivo");
        } else {
            setEstatus("Activo");
        }
    }

    const saveCategoria = async (id) => {
        if (nombre.length > 0) {
            let data = {
                nombre: nombre,
                estatus: estatus,
            };
            console.log(data);
            CategoriaDataService.update(id, data)
                .then(response => {
                    Swal.fire('Actualizado correctamente', '', 'success');
                    props.funcion();
                })
                .catch(e => {
                    console.log(e);
                    Swal.fire('Ha ocurrido un problema', e, 'info');
                });
        } else if (nombre.length == 0) {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No se ha ingresado ningun nombre",
                showConfirmButton: false,
                timer: 1500,
            });
        }
    }

    const confirmarModificar = (id) => {
        Swal.fire({
            title: 'Modificar',
            text: 'Esta categoría será modificada, ¿desea continuar?',
            icon: 'warning',
            buttons: {
                confirm: { text: 'Ubah', className: 'sweet-warning' },
                cancel: 'Batalkan'
            },
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            closeOnConfirm: true,
            confirmButtonColor: "#1976d2"
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                setBlock(true);
                saveCategoria(id);
            }
        })
    };

    useEffect(() => {
        setEstatus(props.categoria.estatus);
        setNombre(props.categoria.nombre);
        setBlock(false);
    }, [props]);






    return (
        <div>
            <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">Actualizar categoría</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div className="row">
                                <div className="col-sm-12 col-md-12 col-lg-12">
                                    <div>
                                        <div className="form-floating mb-3">
                                            <input type="text" className="rounded-item form-control" id="floatingInput" placeholder="Nombre Categoría" value={nombre} onChange={changeName} disabled={block} />
                                            <label htmlFor="floatingInput">Nombre Categoría</label>
                                        </div>
                                    </div>
                                    {/* este switch solo aparece cuando la categoria está inactiva */}
                                    <div id="switch" className="form-check form-switch">
                                        <input className="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked={estatus == "Activo" ? "true" : ""} disabled={block} onChange={changeEstatus} />
                                        <label className="form-check-label" for="flexSwitchCheckChecked">Categoria habilitada</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button onClick={() => confirmarModificar(props.categoria._id)} className="btn btn-warning" id="btnModificar" data-bs-dismiss="modal">Modificar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

ModalActualizar.propTypes = {
    categoria: PropTypes.object,
    funcion: PropTypes.func,
}

export default ModalActualizar
