
import React, { useEffect, useState } from 'react'
import TituloSeccion from "../titulos/TituloSeccion";
import "../estilos/categoria.css";
import ContainerTabla from './ContainerTabla';
import ModalActualizar from './ModalActualizar';
import CategoriaDataService from "../../services/categoria.service";
import ModalAgregar from './ModalAgregar';

const ContenedorCategoria = () => {
    var rol = localStorage.getItem('rol');
    if (rol === "2") {
        localStorage.clear();
        window.location.href = "/login";
    }

    const [categoria, setcategoria] = useState([]);
    const [agregado, setagregado] = useState(false)

    const retrieveCategorias = async () => {
        CategoriaDataService.getAll()
            .then(response => {
                setcategoria(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    useEffect(() => {
        retrieveCategorias()
        setagregado(false)
    }, [agregado])



    return (
        <div className="col py-3 ">
            <div className="row" id="contenedor">
                <div className="col-sm-12 col-md-12 col-lg-9">
                    <TituloSeccion nombreSeccion="Categoría" />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-3 mb-3">
                    <button data-bs-toggle="modal" data-bs-target="#staticBackdrop1" className="btn btn" id="btnAgregar"><i className="bi bi-plus-lg"></i>Agregar Nueva</button>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <ContainerTabla categorias={categoria} funcion={retrieveCategorias} />
                </div>
                <div id="" className="col-sm-12 col-md-12 col-lg-12">
                    <ModalAgregar setagregado={setagregado} />

                </div>
            </div>
        </div>
    );
};

export default ContenedorCategoria
