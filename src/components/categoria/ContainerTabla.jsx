import React, { Component,useState,useEffect} from 'react'
import BarraBusqueda from '../barraBusqueda/BarraBusqueda';
import "../estilos/categoria.css";
import Swal from 'sweetalert2';
import CategoriaDataService from "../../services/categoria.service";

import ModalActualizar from './ModalActualizar';

const ContainerTabla = ({categorias,funcion}) => {
  const [categoriaS, setCategoriaS] = useState([]);
  const [modalcheck,setModalCheck] = useState({});

  const confirmarEliminar = (id) => {
    Swal.fire({
      title: 'Eliminar',
      text: 'Esta categoría será eliminada, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#b71c1c "
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        deleteCategoria(id);
      }
    })
  };

  const deleteCategoria = (id) => {

    CategoriaDataService.delete(id)
        .then(response => {
            Swal.fire('Eliminado exitosamente', '', 'success');
            funcion();
        })
        .catch(e => {
            console.log(e);
            Swal.fire('Ha ocurrido un problema', e, 'info');
        });
  }

  const handleChange = (e) => {
    let enter = e.target.value;
    let busqueda = categoriaS.filter((item) =>
     item.nombre.toString().toLowerCase().includes(enter) );

    if (enter === "") {
      setCategoriaS(categorias);
    } else {
      setCategoriaS(busqueda);
    }
  };
  
  useEffect(() => {
    setCategoriaS(categorias)
  }, [categorias]);

  return (

    <div>
      <ModalActualizar categoria={modalcheck} funcion={funcion}/>
      <div className="card" id="container">
      
      <div className="card-body">
        <div className="row">
          <div className="col-sm-12 col-md-12 col-lg-8"></div>
            <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                <input
                  placeholder="buscar"
                  type="text"
                  style={{ height: "50px" }}
                  onChange={handleChange}
                  name="buscar"
                  id="buscar"
                  className="form-control rounded-item"
                />
                <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                </label>
              </div>
            </div>
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-12">
              <table className="table">
                <thead className="thead-light">
                  <tr id="titulo">
                    <th scope="col">Nombre Categoría</th>
                    <th scope="col">Estatus</th>
                    <th colSpan="2" scope="col">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  {categoriaS &&
                    categoriaS.map((categoria) => (
                      <tr key={categoria._id}>
                        <td  >
                          {categoria.nombre}</td>
                        <td>
                          {categoria.estatus}</td>
                        <td><button onClick={() => confirmarEliminar(categoria._id)} className="btn btn-outline-danger" id=""><i className="bi bi-trash"></i></button></td>
                        <td><button onClick={() =>{
                          console.log(categoria);
                          setModalCheck(categoria);
                          console.log(modalcheck);
                        }} data-bs-target="#staticBackdrop" className="btn btn-outline-warning" data-bs-toggle="modal" data-bs-target="#staticBackdrop"><i className="bi bi-pencil-square"></i></button></td>
                        
                      </tr>
                      
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    </div>
    
  )
}

export default ContainerTabla



