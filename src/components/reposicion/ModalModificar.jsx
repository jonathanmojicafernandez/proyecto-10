import React, { useState, useEffect } from "react";

const ModalModificar = ({ reposicion, setCambio }) => {
  const [entradas_prestamo, setentradas_prestamo] = useState([]);
  const [entradas_eliminar, setEntradas_eliminar] = useState([]);

  const [usuario, setUsuario] = useState({});
  const [clave, setClave] = useState("");
  const [nombre, setNombre] = useState("");
  const [entradaM, setentradaM] = useState({});
  useEffect(() => {
    if (reposicion._id != null || reposicion._id !== undefined) {
      setentradas_prestamo(reposicion.entradas_prestamo);
      setEntradas_eliminar([]);

      setUsuario(reposicion.usuario);
    } else {
      console.log("No hay datos en el producto");
    }
  }, [reposicion]);
  return (
    <div
      className="modal fade"
      id="staticBackdrop"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabindex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-lg">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Detalle de reposicion
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>

          <div class="modal-body">
            <div class="row mb-3">
              <div className="col">
                <strong>Matricula</strong>
              </div>
              <div className="col">
                <p>{usuario.matricula}</p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Solicitante</strong>
              </div>
              <div className="col">
                <p>
                  {usuario.nombres} {usuario.apellido1} {usuario.apellido2}
                </p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Fecha inicio</strong>
              </div>
              <div className="col">
                <p>{reposicion.inicio}</p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Fecha de entrega</strong>
              </div>
              <div className="col">
                <p>{reposicion.fin}</p>
              </div>
            </div>
            <div className="row mt-3  table-responsive">
              <table className="table table-hover">
                <thead>
                  <tr id="titulo" className="border-remove">
                    <th scope="col">Clave</th>
                    <th scope="col">Material</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Acuerdo</th>
                  </tr>
                </thead>
                <tbody>
                  {entradas_prestamo &&
                    entradas_prestamo.map((entrada) => (
                      <tr key={entrada._id}>
                        <td>{entrada.clave}</td>
                        <td>{entrada.material.nombre}</td>
                        <td>${entrada.costo}</td>
                        <td>{entrada.incidencia.acuerdo}</td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>

            <br />
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Cerrar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalModificar;
