import React, { useEffect, useState } from 'react'

import ReposicionService from '../../services/reposicion.service';

import TituloSeccion from '../titulos/TituloSeccion';
import TablaReposicion from './TablaReposicion';

const ContenedorReposicion = () => {
    var rol = localStorage.getItem('rol');
    if (rol === "3") {
        localStorage.clear();
        window.location.href = "/login";
    }

    const [reposiciones, setReposiciones] = useState([]);
    const [cambio, setCambio] = useState(false);
    const retrieveReposiciones = async () => {
        let response = await ReposicionService.getAll().then();
        setReposiciones(response.data);
    };

    useEffect(() => {
        retrieveReposiciones();
    }, [cambio]);
    return (
        <div className="col py-3 ">
            <div className="row" id="contenedor">
                <div className="col-sm-12 col-md-12 col-lg-9">
                    <TituloSeccion nombreSeccion="Reposicion" />
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <TablaReposicion reposiciones={reposiciones} setCambio={setCambio} />
                </div>
            </div>
        </div>
    )
}

export default ContenedorReposicion
