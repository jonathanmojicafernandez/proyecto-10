import React, {useState, useEffect} from 'react'
import BarraBusqueda from '../barraBusqueda/BarraBusqueda'
import ModalModificar from './ModalModificar';

import prestamosService from '../../services/prestamos.service';

import EntradaDataService from "../../services/entrada.service";
import Swal from "sweetalert2";

const TablaReposicion = ({reposiciones, setCambio}) => {
  const [reposicionS, setReposicionS] = useState([]);
  const [reposicionSel, setReposicionSel] = useState({});
  const seleccionReposicion = (reposi) => {
    setReposicionSel(reposi);
  };
  const confirmarDev = (id, ent) => {
    Swal.fire({
      title: 'Entregar',
      text: 'Esta reposicion ha sido realizada correctamente, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#1976d2 "
    }).then((result) => {
      if (result.isConfirmed) {
        realizarDev(id, ent);
      }
    })
  };
  const realizarDev = async (id, ent) => {
    let data = {
      idPrestamo: id
    };
    let response = await prestamosService.devolucion(data).then();
    if (response.status === 200) {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Se ha finalizado correctamente la reposicion',
        showConfirmButton: false,
        timer: 1500
      });

    }
    cambiarEstatus(ent);
  };
  const cambiarEstatus = async (ent) => {
    let data = {
      estatus: "Disponible",
      entradas_prestamo: ent
    };
    let response = await EntradaDataService.changeStatus(data).then();
    if (response.status === 200) {
    }
    limpiar();
  };
  const limpiar = () => {
    setReposicionSel({});
    setCambio(true);
  };
  
  const handleChange = (e)=>{
    let enter = e.target.value;
    let busqueda = reposicionS.filter((item) =>
     item.usuario.matricula.toString().toLowerCase().includes(enter)||
     item.usuario.nombres.toString().toLowerCase().includes(enter)||
     item.usuario.apellido1.toString().toLowerCase().includes(enter)||
     item.usuario.apellido2.toString().toLowerCase().includes(enter) );
    if (enter === "") {
      setReposicionS(reposiciones);
    } else {
      setReposicionS(busqueda);
    }
  }; 
  useEffect(() => {
    setReposicionS(reposiciones)
  }, [reposiciones]);
    return (
        <div>
         <ModalModificar  reposicion={reposicionSel} setCambio={setCambio} />
        <div className="card" id="container">
          <div className="card-body">
            <div className="row">
              <div className="col-sm-12 col-md-12 col-lg-8"></div>
              <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                  <input
                    placeholder="buscar"
                    type="text"
                    style={{ height: "50px" }}
                    onChange={handleChange}
                    name="buscar"
                    id="buscar"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                  </label>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                  <table className="table table-responsive table-hover">
                    <thead>
                      <tr id="titulo" className="border-remove">
                        <th scope="col">Matricula</th>
                        <th scope="col">Solicitante</th>
                        <th scope="col">Inicio</th>
                        <th scope="col">Fin</th>
                        <th colSpan="3" scope="col">
                          Acciones
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    {reposicionS &&
                      reposicionS.map((reposicion) => (
                        <tr key={reposicion._id}>
                          <td>{reposicion.usuario.nombres} {reposicion.usuario.apellido1} {reposicion.usuario.apellido2}</td>
                          <td>{reposicion.usuario.matricula}</td>
                          <td>{reposicion.inicio}</td>
                          <td>{reposicion.fin}</td>
                          <td>
                            <button
                              onClick={() => {
                                confirmarDev(reposicion._id, reposicion.entradas_prestamo);
                              }}
                              className="btn btn-outline-success"
                              id=""
                            >
                              <i class="bi bi-check2-square"></i>
                            </button>
                          </td>
                          <td>
                            <button
                              onClick={() => {
                                seleccionReposicion(reposicion);
                              }}
                              data-bs-target="#staticBackdrop"
                              className="btn btn-outline-info"
                              data-bs-toggle="modal"
                              data-bs-target="#staticBackdrop"
                            >
                              <i class="bi bi-info-circle"></i>
                            </button>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TablaReposicion
