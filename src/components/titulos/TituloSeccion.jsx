import React from 'react'
import PropTypes from 'prop-types';

const TituloSeccion =({nombreSeccion}) => {
    return (
        <div>
            <h1>{nombreSeccion}</h1>
        </div>
    )
}
TituloSeccion.propTypes={
    nombreSeccion: PropTypes.string
};
export default TituloSeccion
