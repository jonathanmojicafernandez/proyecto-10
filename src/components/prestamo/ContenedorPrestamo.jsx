import React, { useState, useEffect } from 'react'
import TituloSeccion from '../titulos/TituloSeccion'
import ModalAgregar from './ModalAgregar'
import TablaPrestamos from './TablaPrestamos';
import PrestamosService from '../../services/prestamos.service';


const ContenedorPrestamo = () => {
    var rol = localStorage.getItem('rol');
    
    const [cambio, setCambio] = useState(false);
    const [prestamos, setPrestamos] = useState([]);

    const retrievePrestamo = async () => {
        let reponse = await PrestamosService.getAll().then();
        setPrestamos(reponse.data);
    };

    useEffect(() => {
        retrievePrestamo();
        setCambio(false);
    }, [cambio])
    return (
        <div className="col py-3 ">
            <div className="row" id="contenedor">
                <div className="col-sm-12 col-md-12 col-lg-9">
                    <TituloSeccion nombreSeccion="Prestamos" />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-3  mb-3">
                    {rol === "2" ?
                        <button data-bs-toggle="modal" data-bs-target="#staticBackdrop1" className="btn btn" id="btnAgregar"><i className="bi bi-plus-lg"></i> Agregar Nuevo</button>
                        : null
                    }
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <TablaPrestamos prestamos={prestamos} setCambio={setCambio} />
                </div>

                <ModalAgregar setCambio={setCambio} />
            </div>
        </div>
    )
}

export default ContenedorPrestamo
