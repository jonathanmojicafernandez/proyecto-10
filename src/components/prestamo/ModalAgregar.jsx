import React, { useState, useEffect } from "react";
import "../estilos/categoria.css";
import "../estilos/tablas.css";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../estilos/datepiker.css";

import Swal from "sweetalert2";

import EntradaDataService from "../../services/entrada.service";
import prestamosService from "../../services/prestamos.service";
import usuarioDataService from "../../services/usuario.service";

const ModalAgregar = ({ setCambio }) => {
  const [entradas_prestamo, setentradas_prestamo] = useState([]);
  const [endDate, setEndDate] = useState(new Date());
  const [startDate, setStartDate] = useState();

  const [nombre, setNombre] = useState("");
  const [entradaM, setentradaM] = useState({});
  const [clave, setClave] = useState("");

  const [idu, setId] = useState(0);
  const [user, setUsuario] = useState({});
  const [matricula, setMatricula] = useState("");
  const [idM, setIdM] = useState(0);

  const [fin, setFin] = useState();

  const holidays = [new Date(2021, 12, 19)];

  const addDays = (date, max) => {
    date.setDate(date.getDate() + max);
    return date;
  };

  const buscarMaterial = async () => {

    try {
      let idL =  JSON.parse(localStorage.getItem("idU"));
      let data = {
        id: idL,
        clave: clave
      };
      let response = await EntradaDataService.search(data).then();
      console.log(response.data);
      if (response.status === 200) {
        setNombre(response.data.entrada.material.nombre);
        setentradaM(response.data.entrada);
        setIdM(response.data.entrada._id);
      }
    } catch (err) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha encontrado este material o no pertenece a este laboratorio",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };

  const buscarUsuario = async () => {
    
      let data = {
        matricula: matricula,
      };
      let response = await usuarioDataService.searchUser(data).then();
      console.log();
      if (response.data.length > 0) {
        setUsuario(response.data[0]);
        setId(response.data[0]._id);
      }else{
        Swal.fire({
          position: "center",
          icon: "info",
          title: "No se ha encontrado este registro",
          showConfirmButton: false,
          timer: 1500,
        });
      }
   
  };

  const agregarMaterial = () => {
   if(entradaM.hasOwnProperty("_id") !== false){
    const encontrar = entradas_prestamo.filter((item) => item._id === idM);
    if (encontrar.length === 0) {
      let entra = {
        _id: entradaM._id,
        costo: entradaM.costo,
        clave: entradaM.clave,
        estatus: entradaM.estatus,
        laboratorio: entradaM.laboratorio,
        material: entradaM.material,
        inicio: startDate,
        fin: fin,
      };
      setentradas_prestamo([...entradas_prestamo, entra]);
    } else {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "Esta agregando el mismo material",
        showConfirmButton: false,
        timer: 1500,
      });
    }

   }else{
    Swal.fire({
      position: "center",
      icon: "info",
      title: "No has elegido un material aun",
      showConfirmButton: false,
      timer: 1500,
    });
   }
    
  };

  const quitar = (id) => {
    const elemnts = entradas_prestamo.filter((item) => item._id !== id);
    setentradas_prestamo([...elemnts]);
  };

  const agregarPrestamo = async () => {
    let dd = endDate.getDate();
    let mm = parseInt(endDate.getMonth()) + 1;
    let yyyy = endDate.getFullYear();
    let fec = yyyy + "-" + mm + "-" + dd;
    let data = {
      inicio: startDate,
      fin: fec,
      idUsuario: idu,
      entradas_prestamo: entradas_prestamo,
    };
    if(entradas_prestamo.length > 0){
      let response = await prestamosService.create(data).then();
    if (response.data.message === "Se ha registrado correctamente") {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Se registrado correctamente el prestamo",
        showConfirmButton: false,
        timer: 1500,
      });

      cambiarEstatusP();
    }

    if (
      response.data.message === "Cuenta con adeudos o reposiciones o prestamos"
    ) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "Cuenta con adeudos, reposiciones o un prestamo",
        showConfirmButton: false,
        timer: 1500,
      });
    }

    }else{
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No ha ingresado ningun material",
        showConfirmButton: false,
        timer: 1500,
      });
    }
    
  };

  const cambiarEstatusP = async () => {
    let datos = {
      estatus: "Prestado",
      entradas_prestamo: entradas_prestamo,
    };
    let response = await EntradaDataService.changeStatus(datos).then();
    if (response.status === 200) {
    }

    setTimeout(function () {
      window.location.reload(true);
    }, 1000);
  };

  const isWeekday = (date) => {
    const day = date.getDay();
    return day !== 0 && day !== 6;
  };

  useEffect(() => {
    let d = new Date();
    let dd = d.getDate();
    let mm = parseInt(d.getMonth()) + 1;
    let yyyy = d.getFullYear();
    let fec = yyyy + "-" + mm + "-" + dd;
    setStartDate(fec);
  }, []);

  return (
    <div
      className="modal fade"
      id="staticBackdrop1"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabindex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog  modal-lg">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Realizar prestamo
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <div class="row">
              <div className="col">
                <div className="form-floating mb-3 input-group-sm">
                  <input
                    placeholder="Matricula"
                    type="number"
                    style={{ height: "50px" }}
                    min="1"
                    pattern="^[0-9]+"
                    onChange={(e) => setMatricula(e.target.value)}
                    id="matricula"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="lab">
                    Matricula
                  </label>
                </div>
              </div>
              <div className="col">
                <button className="btn mt-2 btn-light" onClick={buscarUsuario}>
                  <i class="bi bi-search"></i>
                </button>
              </div>
            </div>
            <br />
            <h6>Datos del solicitante</h6>
            <hr />
            <div className="row">
              <div className="col">
                <label className="floatingInput" htmlFor="localizacion">
                  Nombres
                </label>
                <input
                  id="nombre"
                  className="form-control rounded-item"
                  value={user.nombres}
                  name="nombre"
                  type="text"
                  disabled
                />
              </div>
              <div className="col">
                <label className="floatingInput" htmlFor="localizacion">
                  Apellido paterno
                </label>
                <input
                  id="nombre"
                  className="form-control rounded-item"
                  value={user.apellido1}
                  name="nombre"
                  type="text"
                  disabled
                />
              </div>
              <div className="col">
                <label className="floatingInput" htmlFor="localizacion">
                  Apellido materno
                </label>
                <input
                  id="nombre"
                  className="form-control rounded-item"
                  value={user.apellido2}
                  name="nombre"
                  type="text"
                  disabled
                />
              </div>
              <div className="col">
                <div class="text-center">
                  <img src="..." class="rounded" alt="..." />
                </div>
              </div>
            </div>

            <br />
            <h6>Detalles del prestamo</h6>
            <hr />
            <div class="row">
              <div className="col">
                <div className="form-floating mb-3 input-group-sm">
                  <input
                    placeholder="Clave de material"
                    type="text"
                    style={{ height: "50px" }}
                    onChange={(e) => {
                      setClave(e.target.value);
                    }}
                    id="clave"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="lab">
                    Clave de material
                  </label>
                </div>
              </div>
              <div className="col">
                <button className="btn mt-2 btn-light" onClick={buscarMaterial}>
                  <i class="bi bi-search"></i>
                </button>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <label className="floatingInput" htmlFor="localizacion">
                  Nombre del material
                </label>
                <input
                  id="nombre"
                  className="form-control rounded-item"
                  value={nombre}
                  name="nombre"
                  type="text"
                  disabled
                />
              </div>
              <div className="col mt-4">
                <button
                  className="btn "
                  id="btnAgregar"
                  onClick={agregarMaterial}
                >
                  <i class="bi bi-plus-lg"></i>
                </button>
              </div>
            </div>
            <div class="row  align-items-center ms-1 mb-3 mt-3">
              <div className="col">
                <p>Fecha de inicio</p>
                <p>
                  <strong>{startDate}</strong>
                </p>
              </div>
            </div>
            <div className="row ms-1 mb-3 mt-3 align-items-center">
              <div class="col-6">
                <p>Fecha de entrega</p>
                <div class="input-group rounded-item">
                  <i class="input-group-text bi bi-calendar3"></i>
                  <div className="form-control ">
                    <DatePicker
                      selected={endDate}
                      onChange={(date) => setEndDate(date)}
                      minDate={new Date()}
                      maxDate={addDays(new Date(), 14)}
                      filterDate={isWeekday}
                      excludeDates={holidays}
                      dateFormat="yyyy-MM-dd"
                      className="sinBorder col-12 "
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-5 ms-2 me-2">
              <table className="table table-responsive table-hover">
                <thead>
                  <tr id="titulo" className="border-remove">
                    <th scope="col">Clave</th>
                    <th scope="col">Material</th>
                    <th colSpan="2" scope="col">
                      {" "}
                      Acciones{" "}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {entradas_prestamo &&
                    entradas_prestamo.map((entrada) => (
                      <tr key={entrada._id}>
                        <td>{entrada.clave}</td>
                        <td>{entrada.material.nombre}</td>
                        <td>
                          <button
                            className="btn btn-outline-danger"
                            onClick={() => quitar(entrada._id)}
                          >
                            <i class="bi bi-x-lg"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Cancelar
            </button>
            <button
              onClick={agregarPrestamo}
              className="btn btn-success"
              data-bs-dismiss="modal"
              id="btnModificar"
            >
              Agregar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalAgregar;
