import React, { useState, useEffect } from 'react'
import PrestamosService from '../../services/prestamos.service';
import EntradaDataService from "../../services/entrada.service";

import Swal from "sweetalert2";
import ModalActualizar from './ModalActualizar';
import ModalReposicion from './ModalReposicion';

const TablaPrestamos = ({ prestamos, setCambio }) => {
  var rol = localStorage.getItem('rol');
  const [prestamoSel, setPrestamoSel] = useState({});
  const [prestamosS, setPrestamosS] = useState([]);

  const seleccionPrestamo = (prestamo) => {
    setPrestamoSel(prestamo);
  };

  const confirmarDev = (id, ent) => {
    Swal.fire({
      title: 'Entrega',
      text: 'Este prestamo finalizara, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#1976d2 "
    }).then((result) => {
      if (result.isConfirmed) {
        realizarDev(id, ent);
      }
    })
  };
  const realizarDev = async (id, ent) => {
    let data = {
      idPrestamo: id
    };
    let response = await PrestamosService.devolucion(data).then();
    if (response.status === 200) {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Se ha finalizado correctamente el prestamo',
        showConfirmButton: false,
        timer: 1500
      });

    }
    cambiarEstatus(ent);
  };
  const cambiarEstatus = async (ent) => {
    let data = {
      estatus: "Disponible",
      entradas_prestamo: ent
    };
    let response = await EntradaDataService.changeStatus(data).then();
    if (response.status === 200) {
    }
    limpiar();
  };
  const limpiar = () => {
    setPrestamoSel({});
    setCambio(true);
  };


  const handleChange = (e) => {
    let enter = e.target.value;
    let busqueda = prestamosS.filter((item) =>
     item.usuario.matricula.toString().toLowerCase().includes(enter)||
     item.usuario.nombres.toString().toLowerCase().includes(enter)||
     item.usuario.apellido1.toString().toLowerCase().includes(enter)||
     item.usuario.apellido2.toString().toLowerCase().includes(enter) );
    if (enter === "") {
      setPrestamosS(prestamos);
    } else {
      setPrestamosS(busqueda);
    }
  };
  useEffect(() => {
    setPrestamosS(prestamos)
  }, [prestamos]);
  
    return (
        <div>
       <ModalActualizar prestamo={prestamoSel} setCambio={setCambio} />
       <ModalReposicion reposicion={prestamoSel} setCambio={setCambio} />
        <div className="card" id="container">
          <div className="card-body">
            <div className="row">
              <div className="col-sm-12 col-md-12 col-lg-8"></div>
              <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                <input
                  placeholder="buscar"
                  type="text"
                  style={{ height: "50px" }}
                  onChange={handleChange}
                  name="buscar"
                  id="buscar"
                  className="form-control rounded-item"
                />
                <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                </label>
              </div>
            </div>
            <div className="row  table-responsive table_chiquita">
              <div className="col-sm-12 col-md-12 col-lg-12  table-responsive">
                <table className="table table-hover">
                  <thead>
                    <tr id="titulo" className="border-remove">
                      <th scope="col">Solicitante</th>
                      <th scope="col">Matricula</th>
                      <th scope="col">Inicio</th>
                      <th scope="col">Fin</th>
                      {rol === "2" ?
                        <th colSpan="3" scope="col">
                          Acciones
                        </th>
                        : null
                      }
                    </tr>
                  </thead>
                  <tbody>
                    {prestamosS &&
                      prestamosS.map((prestamo) => (
                        <tr key={prestamo._id}>
                          <td>{prestamo.usuario.nombres} {prestamo.usuario.apellido1} {prestamo.usuario.apellido2}</td>
                          <td>{prestamo.usuario.matricula}</td>
                          <td>{prestamo.inicio}</td>
                          <td>{prestamo.fin}</td>
                          {rol === "2" ?
                            <td>
                              <button
                                onClick={() => {
                                  confirmarDev(prestamo._id, prestamo.entradas_prestamo);
                                }}
                                className="btn btn-outline-success"
                                id=""
                              >
                                <i class="bi bi-check2-square"></i>
                              </button>
                            </td>
                            : null
                          }
                          {rol === "2" ?
                            <td>
                              <button
                                onClick={() => {
                                  seleccionPrestamo(prestamo);
                                }}
                                data-bs-target="#staticBackdrop"
                                className="btn btn-outline-warning"
                                data-bs-toggle="modal"
                              >
                                <i className="bi bi-pencil-square"></i>
                              </button>
                            </td>
                            : null
                          }
                          {rol === "2" ?
                            <td>
                              <button
                                className="btn btn-outline-danger"
                                data-bs-target="#modalReposicion"
                                data-bs-toggle="modal"
                                onClick={() => {
                                  seleccionPrestamo(prestamo);
                                }}

                              >
                                <i class="bi bi-clipboard-x"></i>
                              </button>
                            </td>
                            : null
                          }
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TablaPrestamos
