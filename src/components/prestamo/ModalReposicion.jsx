import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";

import EntradaDataService from "../../services/entrada.service";

import PrestamosService from "../../services/prestamos.service";
const ModalReposicion = ({ reposicion, setCambio }) => {
  const [entradas_prestamo, setentradas_prestamo] = useState([]);
  const [entradas_eliminar, setEntradas_eliminar] = useState([]);

  const [usuario, setUsuario] = useState({});
  const [idR, setId] = useState("");
  const [comentarios, setComentario] = useState([]);

  const quitar = (id) => {
    if (entradas_prestamo.length > 0) {
      const elemnts = entradas_prestamo.filter((item) => item._id !== id);
      setentradas_prestamo([...elemnts]);
      const dispo = entradas_prestamo.filter((item) => item._id === id);
      if (dispo.length === 1) {
        let entra = {
          _id: dispo[0]._id,
          costo: dispo[0].costo,
          clave: dispo[0].clave,
          estatus: dispo[0].estatus,
          laboratorio: dispo[0].laboratorio,
          material: dispo[0].material,
          inicio: dispo[0].inicio,
          fin: dispo[0].fin,
        };
        setEntradas_eliminar([...entradas_eliminar, entra]);
      }
    }
  };
  const cambiarEstatusD = async () => {
    let data = {
      estatus: "Disponible",
      entradas_prestamo: entradas_eliminar,
    };
    let response = await EntradaDataService.changeStatus(data).then();
    if (response.status === 200) {
      console.log("Se realizo");
    }
  };
  const handleChange = (e) => {
    const elemnts = comentarios.filter((item) => item.id === e.target.name);
    if (elemnts.length > 0) {
      const coments = comentarios.filter((item) => item.id !== e.target.name);
      let com = {
        id: e.target.name,
        comentario: e.target.value,
      };
      setComentario([...coments, com]);
    } else {
      let com = {
        id: e.target.name,
        comentario: e.target.value,
      };
      setComentario([...comentarios, com]);
    }
  };
  const confirmarReposicion = () => {
    if (entradas_prestamo.length > 0) {
      Swal.fire({
        title: "Modificar",
        text: "Este prestamo será modificado, ¿desea continuar?",
        icon: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#1976d2 ",
      }).then((result) => {
        if (result.isConfirmed) {
          mandarReposicion();
        }
      });
    } else {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No puede tener un prestamo sin materiales",
        showConfirmButton: false,
        timer: 2000,
      });
      setCambio(true);
    }
  };

  const mandarReposicion = async () => {
    let materialesF = [];
    let vacio = 0;
    if (comentarios.length === entradas_prestamo.length) {
      comentarios.forEach((element) => {
        if (element.comentario.length === 0) {
          vacio++;
        }
      });
    } else {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "Comentarios!!",
        text: "No haz realizado los comentarios pertinentes",
        showConfirmButton: false,
      });
      return;
    }
    if (vacio === 0) {
      for (let i = 0; i < entradas_prestamo.length; i++) {
        const dispo = comentarios.filter(
          (item) => parseInt(item.id) === entradas_prestamo[i]._id
        );
        let comnt = "";
        if (dispo.length === 1) {
          comnt = dispo[0].comentario;
        }

        let entra = {
          _id: entradas_prestamo[i]._id,
          costo: entradas_prestamo[i].costo,
          clave: entradas_prestamo[i].clave,
          estatus: entradas_prestamo[i].estatus,
          laboratorio: entradas_prestamo[i].laboratorio,
          material: entradas_prestamo[i].material,
          inicio: entradas_prestamo[i].inicio,
          incidencia: {
            acuerdo: comnt,
            estatus: "Averiado",
          },
        };
        materialesF.push(entra);
      }
      let data = {
        idPrestamo: idR,
        prestamo: reposicion,
        entradas_prestamo: materialesF,
      };
      let response = await PrestamosService.changeToReposicioin(data).then();
      if (response.status === 200) {
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Se ha cambiado a reposicion!!",
          text: "Para tratar este prestamo dirigete a incumplimiento de prestamos > Reposicion",
          showConfirmButton: true,
        });
      }
      cambiarEstatusD();
      setCambio(true);
    }else{
      Swal.fire({
        position: "center",
        icon: "info",
        title: "Comentarios!!",
        text: "No haz realizado los comentarios pertinentes",
        showConfirmButton: false,
      });
    }
  };

  useEffect(() => {
    if (reposicion._id != null || reposicion._id !== undefined) {
      setentradas_prestamo(reposicion.entradas_prestamo);
      setEntradas_eliminar([]);
      setId(reposicion._id);

      setUsuario(reposicion.usuario);
    } else {
      console.log("No hay datos en el producto");
    }
  }, [reposicion]);
  return (
    <div
      className="modal fade"
      id="modalReposicion"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabindex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-lg">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Detalle de reposicion
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>

          <div class="modal-body">
            <div class="row mb-3">
              <div className="col">
                <strong>Matricula</strong>
              </div>
              <div className="col">
                <p>{usuario.matricula}</p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Solicitante</strong>
              </div>
              <div className="col">
                <p>
                  {usuario.nombres} {usuario.apellido1} {usuario.apellido2}
                </p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Fecha inicio</strong>
              </div>
              <div className="col">
                <p>{reposicion.inicio}</p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Fecha de entrega</strong>
              </div>
              <div className="col">
                <p>{reposicion.fin}</p>
              </div>
            </div>
            <div className="row mt-3  table-responsive">
              <table className="table table-hover">
                <thead>
                  <tr id="titulo" className="border-remove">
                    <th scope="col">Clave</th>
                    <th scope="col">Material</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Comentario de incidencia</th>
                    <th scope="col">Liberar</th>
                  </tr>
                </thead>
                <tbody>
                  {entradas_prestamo &&
                    entradas_prestamo.map((entrada) => (
                      <tr key={entrada._id}>
                        <td>{entrada.clave}</td>
                        <td>{entrada.material.nombre}</td>
                        <td>${entrada.costo}</td>
                        <td>
                          <div class="form-floating">
                            <textarea
                              class="form-control"
                              name={entrada._id}
                              placeholder="Leave a comment here"
                              id="floatingTextarea"
                             
                              onChange={handleChange}
                            ></textarea>
                            <label for="floatingTextarea">Comentario</label>
                          </div>
                        </td>
                        <td>
                          <button
                            className="btn btn-outline-success"
                            onClick={() => quitar(entrada._id)}
                          >
                            <i class="bi bi-check2-square"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>

            <br />
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Cancelar
            </button>
            <button
              className="btn btn-warning"
              id="btnModificar"
              data-bs-dismiss="modal"
              onClick={confirmarReposicion}
            >
              Notificar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalReposicion;
