import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../estilos/datepiker.css";
import Swal from "sweetalert2";

import EntradaDataService from "../../services/entrada.service";
import prestamosService from "../../services/prestamos.service";

const ModalActualizar = ({ prestamo, setCambio }) => {
  const [entradas_prestamo, setentradas_prestamo] = useState([]);
  const [entradas_eliminar, setEntradas_eliminar] = useState([]);
  const [startDate, setStartDate] = useState();
  const [fin, setFin] = useState("");
  const [inicio, setInicio] = useState("");
  const [usuario, setUsuario] = useState({});
  const [clave, setClave] = useState("");
  const [nombre, setNombre] = useState("");
  const [entradaM, setentradaM] = useState({});
  const [idM, setIdM] = useState(0);

  const limpiar = () => {
    setFin("");
    setInicio("");
    setEntradas_eliminar([]);
    setentradas_prestamo([]);
    setClave("");
    setNombre("");
    setentradaM({});
  };

  
  const buscarMaterial = async () => {

    try {
      let idL =  JSON.parse(localStorage.getItem("idU"));
      let data = {
        id: idL,
        clave: clave
      };
      let response = await EntradaDataService.search(data).then();
      console.log(response.data);
      if (response.status === 200) {
        setNombre(response.data.entrada.material.nombre);
        setentradaM(response.data.entrada);
        setIdM(response.data.entrada._id);
      }
    } catch (err) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha encontrado este material o no pertenece a este laboratorio",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };
  const agregarMaterial = () => {
    if(entradaM.hasOwnProperty("_id") !== false){
    const encontrar = entradas_prestamo.filter((item) => item._id === idM);
    if (encontrar.length === 0) {
      let entra = {
        _id: entradaM._id,
        costo: entradaM.costo,
        clave: entradaM.clave,
        estatus: entradaM.estatus,
        laboratorio: entradaM.laboratorio,
        material: entradaM.material,
        inicio: inicio,
        fin: fin,
        incidencia: {
          id: 1,
          acuerdo: "Lo repondra",
          estatus: "Finalizado",
        },
      };
      setentradas_prestamo([...entradas_prestamo, entra]);
    } else {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "Esta agregando el mismo material",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  }else{
    Swal.fire({
      position: "center",
      icon: "info",
      title: "No has elegido un material aun",
      showConfirmButton: false,
      timer: 1500,
    });
   }
  };

  const confirmarUpdate = () => {
    if(entradas_prestamo.length >0 ){
      Swal.fire({
        title: "Modificar",
        text: "Este prestamo será modificado, ¿desea continuar?",
        icon: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#1976d2 ",
      }).then((result) => {
        if (result.isConfirmed) {
          updatePrestamo();
        }
      });

    }else{
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No puede tener un prestamo sin materiales",
        showConfirmButton: false,
        timer: 2000,
      });
      setCambio(true);
    }
   
  };

  const updatePrestamo = async () => {
    let fecF;
    if (fin === startDate) {
      fecF = fin;
    } else {
      fecF = startDate;
    }

    let d = new Date(fecF);
    let dd = String(d.getDate()).padStart(2, "0");
    let mm = String(d.getMonth() + 1).padStart(2, "0"); //January is 0!
    let yyyy = d.getFullYear();
    d = yyyy + "-" + mm + "-" + dd;

    let data = {
      idPrestamo: prestamo._id,
      fin: d,
      entradas_prestamo: entradas_prestamo,
    };
    let response = await prestamosService.update(data).then();
    if(response.status === 200){
        Swal.fire({
            position: "center",
            icon: "success",
            title: "Se modifico correctamente",
            showConfirmButton: false,
            timer: 1500,
          });
    }
    cambiarEstatusD();
    cambiarEstatusP();
    limpiar();
    setCambio(true);
  };

  const quitar = (id) =>{
    const elemnts = entradas_prestamo.filter(item => item._id !== id);
    setentradas_prestamo([...elemnts]);
    const dispo = entradas_prestamo.filter(item => item._id === id);
    if (dispo.length === 1) {
      let entra = {
        _id: dispo[0]._id,
        costo: dispo[0].costo,
        clave: dispo[0].clave,
        estatus: dispo[0].estatus,
        laboratorio: dispo[0].laboratorio,
        material: dispo[0].material,
        inicio: inicio,
        fin: fin,
        incidencia: {
          id: 1,
          acuerdo: "Lo repondra",
          estatus: "Finalizado",
        },
      };
      setEntradas_eliminar([...entradas_eliminar, entra]);
    }
    

  };
  const cambiarEstatusP = async () =>{
    let data = {
      estatus: "Prestado",
      entradas_prestamo: entradas_prestamo
    };
    let response = await EntradaDataService.changeStatus(data).then();
    if(response.status === 200){
      console.log("Se realizo");
    }
  };

  const cambiarEstatusD = async () =>{
    let data = {
      estatus: "Disponible",
      entradas_prestamo: entradas_eliminar
    };
    let response = await EntradaDataService.changeStatus(data).then();
    if(response.status === 200){
      console.log("Se realizo");
    }
  };



  useEffect(() => {
    if (prestamo._id != null || prestamo._id !== undefined) {
      setentradas_prestamo(prestamo.entradas_prestamo);
      setEntradas_eliminar([]);
      setInicio(prestamo.inicio);
      setUsuario(prestamo.usuario);
      let dia = parseInt(prestamo.fin.substring(8, 10)) + 1;
      let mes = prestamo.fin.substring(5, 7);
      let anio = prestamo.fin.substring(0, 4);
      let fechF = anio + "-" + mes + "-" + dia;
      let date = new Date(fechF);
      setFin(date);
      setStartDate(date);
    } else {
      console.log("No hay datos en el producto");
    }
  }, [prestamo]);
  return (
    <div
      className="modal fade"
      id="staticBackdrop"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabindex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Actualizar material
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>

          <div class="modal-body">
            <div class="row mb-3">
              <div className="col">
                <strong>Matricula</strong>
              </div>
              <div className="col">
                <p>{usuario.matricula}</p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Solicitante</strong>
              </div>
              <div className="col">
                <p>
                  {usuario.nombre}
                  {usuario.apellido1}
                  {usuario.apellido2}
                </p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Fecha inicio</strong>
              </div>
              <div className="col">
                <p>{inicio}</p>
              </div>
            </div>
            <div class="row mb-3">
              <div className="col">
                <strong>Fecha de entrega</strong>
              </div>
              <div className="col">
                <p>{prestamo.fin}</p>
              </div>
            </div>
            <div class="row  align-items-center mb-3">
              <div class="col-6">
                <p>Nueva fecha de entrega</p>
                <div class="input-group rounded-item">
                  <i class="input-group-text bi bi-calendar3"></i>
                  <div className="form-control ">
                    <DatePicker
                      selected={startDate}
                      onChange={(date) => setStartDate(date)}
                      minDate={fin}
                      dateFormat="yyyy-MM-dd"
                      className="sinBorder col-12 "
                    />
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div className="col">
                <div className="form-floating mb-3 input-group-sm">
                  <input
                    placeholder="Clave de material"
                    type="text"
                    style={{ height: "50px" }}
                    onChange={(e) => {
                      setClave(e.target.value);
                    }}
                    id="clave"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="lab">
                    Clave de material
                  </label>
                </div>
              </div>
              <div className="col">
                <button className="btn mt-2 btn-light" onClick={buscarMaterial}>
                  <i class="bi bi-search"></i>
                </button>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <label className="floatingInput" htmlFor="localizacion">
                  Nombre del material
                </label>
                <input
                  id="nombre"
                  className="form-control rounded-item"
                  value={nombre}
                  name="nombre"
                  type="text"
                  disabled
                />
              </div>
              <div className="col mt-4">
                <button
                  className="btn "
                  onClick={agregarMaterial}
                  id="btnAgregar"
                >
                  <i class="bi bi-plus-lg"></i>
                </button>
              </div>
            </div>
            <div className="row mt-3  table-responsive">
              <table className="table table-hover">
                <thead>
                  <tr id="titulo" className="border-remove">
                    <th scope="col">Clave</th>
                    <th scope="col">Material</th>
                    <th scope="col">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  {entradas_prestamo &&
                    entradas_prestamo.map((entrada) => (
                      <tr key={entrada._id}>
                        <td>{entrada.clave}</td>
                        <td>{entrada.material.nombre}</td>
                        <td>
                          <button className="btn btn-outline-danger" onClick={()=> (quitar(entrada._id))}>
                            <i class="bi bi-x-lg"></i>
                          </button>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>

            <br />
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Cancelar
            </button>
            <button
              onClick={confirmarUpdate}
              className="btn btn-warning"
              id="btnModificar"
              data-bs-dismiss="modal"
            >
              Modificar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalActualizar;
