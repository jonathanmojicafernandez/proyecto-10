import React, { useState, useEffect } from 'react'
import "../estilos/categoria.css";
import Swal from 'sweetalert2';
import UsuarioDataService from "../../services/usuario.service";
import rangosDataService from "../../services/rangos.service";

const ModalActualizar = ({ usuario, setagregado, rango }) => {
  const [idUsuario, setIdUsuario] = useState(0);
  const [nombres, setNombres] = useState("");
  const [password, setPassword] = useState("");
  const [apellido1, setApellido1] = useState("");
  const [apellido2, setApellido2] = useState("");
  const [matricula, setMatricula] = useState("");
  const [ciudad, setCiudad] = useState("");
  const [colonia, setColonia] = useState("");
  const [cp, setCp] = useState("");
  const [calle, setCalle] = useState("");
  const [numInt, setNumInt] = useState("");
  const [numExt, setNumExt] = useState("");
  const [estatus, setEstatus] = useState("");
  const [idRango, setRango] = useState(0);

  const [fileInputState, setFileInputState] = useState("");
  const [selectedFile, setSelectedFile] = useState("");
  const [previewSource, setPreviewSource] = useState()
  const [foto, setFoto] = useState("");

  const [rangos, setRangos] = useState([]);

  const retrieveRangos = async () => {
    let response = await rangosDataService.getAll().then();
    setRangos(response.data);
  }


  const handleValidation = () => {

    let formIsValid = true;

    if (apellido2.length > 0) {
        if (!nombres.match(/^[a-zA-Z]+$/) || !apellido1.match(/^[a-zA-Z]+$/) || !apellido2.match(/^[a-zA-Z]+$/)) {
            formIsValid = false
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Solo se pueden ingresar letras',
                showConfirmButton: false,
                timer: 1500
            })
        }
    } else {
        if (!nombres.match(/^[a-zA-Z]+$/) || !apellido1.match(/^[a-zA-Z]+$/)) {
            formIsValid = false
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Solo se pueden ingresar letras',
                showConfirmButton: false,
                timer: 1500
            })
        }
    }
    if (numInt.length > 0) {
        if (!matricula.match(/^[0-9]+$/) || !cp.match(/^[0-9]+$/) || !numInt.match(/^[0-9]+$/) || !numExt.match(/^[0-9]+$/)) {
            console.log("entro if");
            formIsValid = false
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Solo se pueden ingresar numeros',
                showConfirmButton: false,
                timer: 1500
            })
        }
    } else {
        if (!matricula.match(/^[0-9]+$/) || !cp.match(/^[0-9]+$/) || !numExt.match(/^[0-9]+$/)) {
            console.log("entro if");
            formIsValid = false
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Solo se pueden ingresar numeros',
                showConfirmButton: false,
                timer: 1500
            })
        }
    }

    return formIsValid
}
  
  const modificarUsuario = async () => {
    //alert(cp.length);
    
    if (numExt.length > 0 && nombres.length > 0 && apellido1.length > 0 && matricula.length === 8 && ciudad.length > 0 && colonia.length > 0 && cp.length === 5 && calle.length > 0) {
      // && cp.length > 0
      //alert('data.nombres');
      if (handleValidation()) {
        let data = {
          idUsuario: parseInt(idUsuario),
          nombres: nombres,
          apellido1: apellido1,
          apellido2: apellido2,
          matricula: matricula,
          // password: password,
          ciudad: ciudad,
          colonia: colonia,
          cp: parseInt(cp),
          calle: calle,
          numeroInt: numInt,
          numeroExt: numExt,
          // estatus: estatus,
          foto: foto,
          rangoId: parseInt(idRango)
        };
        
        try {
            let response = await UsuarioDataService.update(data).then();
            if (response.status === 200) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Se han guardado los cambios correctamente',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        } catch (error) {
          // alert(error)
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'No se ha podido actualizar la información',
                    showConfirmButton: false,
                })
        }
        setagregado(true);
      }
    } 
    else if (nombres.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ningun nombre",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (apellido1.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ningun apellido",
        showConfirmButton: false,
        timer: 1500,
      });
    }else if (matricula.length != 8) {
      Swal.fire({
          position: "center",
          icon: "info",
          title: "La matricula debe contener 8 caracteres positivos",
          showConfirmButton: false,
          timer: 1500,
      });
  } else if (matricula.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ninguna matricula",
        showConfirmButton: false,
        timer: 1500,
      });
    } 
    // else if (password.length == 0) {
    //   Swal.fire({
    //     position: "center",
    //     icon: "info",
    //     title: "No se ha ingresado ninguna contraseña",
    //     showConfirmButton: false,
    //     timer: 1500,
    //   });
    // } 
    else if (ciudad.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ninguna ciudad",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (colonia.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ninguna colonia",
        showConfirmButton: false,
        timer: 1500,
      });
    }else if (cp.length != 5) {
      Swal.fire({
          position: "center",
          icon: "info",
          title: "El cp debe tener una longitud de 5 positivos",
          showConfirmButton: false,
          timer: 1500,
      });
  } else if (cp.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ningun cp",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (calle.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ninguna calle",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (idRango.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ningun rango",
        showConfirmButton: false,
        timer: 1500,
      });
    }

  };


  const confirmarUsuario = async () => {
    Swal.fire({
      title: 'Modificar',
      text: 'Este usuario será modificado, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#1976d2 "
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        modificarUsuario();
      }
    })

  };

  useEffect(() => {
    setIdUsuario(usuario._id);
    setNombres(usuario.nombres);
    setApellido1(usuario.apellido1);
    setApellido2(usuario.apellido2);
    setMatricula(usuario.matricula);
    setCiudad(usuario.ciudad);
    setColonia(usuario.colonia);
    setCp(usuario.cp);
    setCalle(usuario.calle);
    setNumInt(usuario.numeroInt);
    setNumExt(usuario.numeroExt);
    setEstatus(usuario.estatus);
    setRango(rango.id);
    setFoto(usuario.foto);
    // setPassword(usuario.password)
    retrieveRangos();

  }, [usuario])

  const uploadImage = async (base64EncodedImage) => {
    // alert(base64EncodedImage);
    setFoto(base64EncodedImage);
  }

  const handleFileInputChange = (e) => {
    const file = e.target.files[0];
    previewFile(file);
  }

  const previewFile = (file) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setFoto(reader.result);
    }
  }
  return (
    <div>
      <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">Actualizar usuario</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3 ">
                    <input
                      placeholder="nombres"
                      type="text"
                      onChange={(e) => {
                        setNombres(e.target.value);
                      }}
                      id="lab"
                      className="form-control rounded-item"
                      value={nombres}

                    />
                    <label className="form-label" for="lab">
                      Nombres
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="primer apellido"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setApellido1(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={apellido1}

                    />

                    <label className="form-label" for="local">
                      Primer apellido
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="segudo apellido"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setApellido2(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={apellido2}

                    />

                    <label className="form-label" for="local">
                      Segundo apellido
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="Matricula"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setMatricula(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={matricula}
                      disabled

                    />

                    <label className="form-label" for="local">
                      Matricula
                    </label>
                  </div>
                </div>
                {/* <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="Matricula"
                      type="password"
                      id="local"
                      onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={password}

                    />

                    <label className="form-label" for="local">
                      Password
                    </label>
                  </div>
                </div> */}
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="ciudad"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setCiudad(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={ciudad}

                    />

                    <label className="form-label" for="local">
                      Ciudad
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="colonia"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setColonia(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={colonia}

                    />

                    <label className="form-label" for="local">
                      Colonia
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="CP"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setCp(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={cp}

                    />

                    <label className="form-label" for="local">
                      Cp
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="calle"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setCalle(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={calle}

                    />

                    <label className="form-label" for="local">
                      Calle
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="Num.Int"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setNumInt(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={numInt}

                    />

                    <label className="form-label" for="local">
                      Número Interior
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="Num.Ext"
                      type="text"
                      id="local"
                      onChange={(e) => {
                        setNumExt(e.target.value);
                      }}
                      className="form-control rounded-item"
                      value={numExt}

                    />

                    <label className="form-label" for="local">
                      Número exterior
                    </label>
                  </div>
                </div>
                <div id="" className="col-sm-6 col-md-6 col-lg-6">
                  <div className="form-floating mb-3">
                    <input
                      placeholder="fotografía"
                      name="image"
                      type="file"
                      id="local"
                      onChange={handleFileInputChange}
                      className="form-control rounded-item"
                      value={fileInputState}

                    />

                    <label className="form-label" for="local">
                      Fotografía(opcional)
                    </label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 col-md-12 col-lg-12">
                    {
                      foto && (
                        <img src={foto} alt="chosen" style={{ height: '100%', width: '100%' }} />
                      )
                    }
                  </div>

                </div>
                <div class="row">
                  <div className="col-sm-6 col-md-6 col-lg-6">
                    <div className="form-floating mb-3">
                      <input
                        placeholder="rol actual"
                        type="text"
                        id="local"
                        className="form-control rounded-item"
                        value={rango.nombre}
                        disabled
                      />

                      <label className="form-label" for="local">
                        Rango actual
                      </label>

                    </div>
                  </div>
                  <div id="cmbRango" className="col-sm-6 col-md-6 col-lg-6">
                    <label className="floatingInput" htmlFor="localizacion">Nuevo rango</label>
                    <select onChange={(e) => { setRango(e.target.value); }} className="form-control rounded-item" name="responsable" id="cmbResponsable">
                      <option value={rango.id}>Selecciona una opción</option>
                      <option value="3">Administrador</option>
                      <option value="2">Laboratorista</option>
                      {/* {rangos.map((rango) => {
                        return (
                          <option key={rango.id} value={rango.id}>{rango.nombre}</option>
                        );
                      })} */}
                    </select>
                  </div>
                </div>

              </div>
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button onClick={confirmarUsuario} className="btn btn-warning" id="btnModificar" data-bs-dismiss="modal">Modificar</button>
            </div>
          </div>
        </div>
      </div>
    </div >
  )
}

export default ModalActualizar

