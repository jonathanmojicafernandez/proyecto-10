import React, { useState, useEffect } from 'react'
import "../estilos/usuario.css";
import Swal from 'sweetalert2';
import usuarioDataService from "../../services/usuario.service";

const ModalDetalles = ({ usuario, setagregado, rango }) => {
  const [nombre, setNombre] = useState("");
  const [status, setEstatus] = useState("");
  const [foto, setFoto] = useState("");


  // const retrieveUsuarios = async () => {
  //   let response = await usuarioDataService.getNoFilter().then();
  //   // setUsuario(response.data);
  // }

  const confirmarRechazar = (id) => {
    Swal.fire({
      title: "Rechazar",
      text: "Este usuario será rechazado, ¿desea continuar?",
      icon: "warning",
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
      confirmButtonColor: "#b71c1c ",
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        rechazarUsuario(id);
      }
    });
  };

  const rechazarUsuario = async (id) => {
    let data = {
      idUsuario: id,
    };
    let response = await usuarioDataService.rejected(data).then();
    if (response.status === 200) {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "El usuario se ha rechazado correctamente",
        showConfirmButton: false,
        timer: 1500,
      });
    }
    setagregado(true);
  };

  const confirmarAceptar = (id) => {
    Swal.fire({
      title: "Aceptar",
      text: "Este usuario será aceptado, ¿desea continuar?",
      icon: "warning",
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
      confirmButtonColor: "#b71c1c ",
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        aceptarUsuario(id);
      }
    });
  };

  const aceptarUsuario = async (id) => {
    let data = {
      idUsuario: id,
    };
    let response = await usuarioDataService.accepted(data).then();
    if (response.status === 200) {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "El usuario se ha aceptado correctamente",
        showConfirmButton: false,
        timer: 1500,
      });
    }
    setagregado(true);
  };

  useEffect(() => {
    setNombre(usuario.nombres + ' ' + usuario.apellido1 + ' ' + usuario.apellido2);
    setEstatus(usuario.estatus);
    setFoto(usuario.foto);
    // retrieveUsuarios();

  }, [usuario])
  return (
    <div>
      <div className="modal fade" id="staticBackdrop2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">Detalles de usuario</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <div className="form-floating mb-3 ">

                <div className="form-group" id="labels">
                  <h6>{nombre}</h6>
                  <h6>{rango.nombre}</h6>
                </div>

                <div className="col-sm-12 col-md-12 col-lg-12">
                  <div id="divFoto">
                    <img src={foto} alt="" />
                  </div>
                </div>

              </div>
              <br />
            </div>
            <div className="modal-footer">
              {usuario.estatus === "Aceptado" ? 
                <button onClick={() => confirmarRechazar(usuario._id)} className="btn btn-danger" data-bs-dismiss="modal" id="">Rechazar</button> 
                :
                <button onClick={() => confirmarAceptar(usuario._id)} className="btn btn-success" data-bs-dismiss="modal" id="">Aceptar</button>
              }
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div >
  )
}

export default ModalDetalles

