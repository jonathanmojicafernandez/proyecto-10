import React, { useState,useEffect } from "react";
import BarraBusqueda from "../barraBusqueda/BarraBusqueda";
import "../estilos/categoria.css";
import "../estilos/tablas.css";
import Swal from "sweetalert2";
import usuarioDataService from "../../services/usuario.service";
import ModalDetalles from './ModalDetalles';

import ModalActualizar from "./ModalActualizar";

const ContainerTabla = ({ usuarios, funcion, setagregado }) => {
  const [modalcheck, setModalCheck] = useState({});
  const [usuario, setUsuario] = useState({});
  const [display, setdisplay] = useState(false);
  const [rango, setRango] = useState({});
  const [usuario1, setUsuario1] = useState({});
  const [usuario2, setUsuario2] = useState({});
  const [usuarioS, setUsuarioS] = useState([]);

  const handleChange = (e) => {
    let enter = e.target.value;
    let busqueda = usuarioS.filter((item) =>
    item.colonia.toString().toLowerCase().includes(enter) || item.rango.nombre.toString().toLowerCase().includes(enter) || item.estatus.toString().toLowerCase().includes(enter) || item.nombres.toString().toLowerCase().includes(enter) || item.apellido1.toString().toLowerCase().includes(enter) ||
     item.apellido2.toString().toLowerCase().includes(enter)  ||  item.calle.toString().toLowerCase().includes(enter) ||  item.ciudad.toString().toLowerCase().includes(enter) ||  item.matricula.toString().toLowerCase().includes(enter) ||  item.numeroExt.toString().toLowerCase().includes(enter) || item.numeroInt.toString().toLowerCase().includes(enter) || item.cp.toString().toLowerCase().includes(enter));

    if (enter === "") {
      setUsuarioS(usuarios);
    } else {
      setUsuarioS(busqueda);
    }
  };
  
  useEffect(() => {
    setUsuarioS(usuarios)
  }, [usuarios]);

  return (
    <div>
      <ModalActualizar
        usuario={usuario2}
        setagregado={setagregado}
        rango={rango}
      />

      <ModalDetalles
        usuario={usuario1}
        setagregado={setagregado}
        rango={rango}
      />

      <div className="card" id="container" >
        <div className="card-body table-responsive">
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-8"></div>
            <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                <input
                  placeholder="buscar"
                  type="text"
                  style={{ height: "50px" }}
                  onChange={handleChange}
                  name="buscar"
                  id="buscar"
                  className="form-control rounded-item"
                />
                <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                </label>
              </div>
            </div>
          </div>
            <div className="row  table-responsive table_chiquita">
              <div className="col-sm-12 col-md-12 col-lg-12  table-responsive">
                <table className="table table-hover">
                  <thead className="thead_sticky">
                  <tr id="titulo" className="border-remove">
                    <th scope="col">Nombre</th>
                    <th scope="col">Matricula</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Número Int</th>
                    <th scope="col">CP</th>
                    <th scope="col">Estatus</th>
                    <th scope="col">Rol</th>
                    <th colSpan="2" scope="col">
                      Acciones
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {usuarioS &&
                    usuarioS.map((usuario) => (
                      <tr key={usuario._id}>
                        <td>{usuario.nombres} {usuario.apellido1} {usuario.apellido2}</td>
                        <td>{usuario.matricula}</td>
                        <td>{usuario.calle} {usuario.numeroExt} {usuario.colonia}</td>
                        <td>{usuario.numeroInt}</td>
                        <td>{usuario.cp}</td>
                        <td>{usuario.estatus}</td>
                        <td>{usuario.rango.nombre}</td>
                        <td>
                          <button
                            onClick={() => {
                              setUsuario1(usuario);
                              setRango(usuario.rango);
                            }}
                            data-bs-target="#staticBackdrop2"
                            className="btn btn-outline-primary"
                            data-bs-toggle="modal"
                            data-bs-target="#staticBackdrop2"
                          >
                            <i class="bi bi-collection"></i>
                          </button>
                        </td>
                        <td>
                          <button
                            onClick={() => {
                              setUsuario2(usuario);
                              setRango(usuario.rango)
                            }}
                            data-bs-target="#staticBackdrop"
                            className="btn btn-outline-warning"
                            data-bs-toggle="modal"
                            data-bs-target="#staticBackdrop"
                          >
                            <i className="bi bi-pencil-square"></i>
                          </button>
                        </td>

                      </tr>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContainerTabla;
