
import React, { useEffect, useState } from 'react'
import TituloSeccion from "../titulos/TituloSeccion";
import "../estilos/categoria.css";
import ContainerTabla from './ContainerTabla';
import ModalActualizar from './ModalActualizar';
import usuarioDataService from "../../services/usuario.service";
import ModalAgregar from './ModalAgregar';
import ModalDetalles from './ModalDetalles';

const ContenedorUsuario = () => {
    var rol = localStorage.getItem('rol');
    if (rol === "2") {
        localStorage.clear();
        window.location.href = "/login";
    }

    const [usuario, setUsuario] = useState([]);
    const [agregado, setagregado] = useState(false)

    const retrieveUsuarios = async () => {
        let response = await usuarioDataService.getNoFilter().then();
        setUsuario(response.data);
        // console.log(response)
    }


    useEffect(() => {
        retrieveUsuarios()
        setagregado(false)
    }, [agregado])


    return (
        <div className="col py-3 ">
            <div className="row" id="contenedor">
                <div className="col-sm-12 col-md-12 col-lg-9">
                    <TituloSeccion nombreSeccion="Usuarios" />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-3 mb-3">
                    <button data-bs-toggle="modal" data-bs-target="#staticBackdrop1" className="btn btn" id="btnAgregar"><i className="bi bi-plus-lg"></i>Agregar Nuevo</button>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <ContainerTabla usuarios={usuario} funcion={retrieveUsuarios} setagregado={setagregado} />
                </div>
                <div id="" className="col-sm-12 col-md-12 col-lg-12">
                    <ModalAgregar setagregado={setagregado} />

                </div>
            </div>
        </div>

    );
};

export default ContenedorUsuario
