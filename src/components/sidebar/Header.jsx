import React,{useState} from "react";

import logo from "../../res/logo.png";
import "../estilos/header.css";

const Header = () => {
  const [name] = useState(localStorage.getItem('nombre'));



  const OnClickEvent = () =>{
    localStorage.clear();
    localStorage.removeItem("log")
    window.location.href = "/";
  }


  return (
    <header className="py-3 mb-2 pe-3  border-bottom shadow  bg-body rounded">
      <div className="container-fluid d-grid gap-3 align-items-center heade">
        <div className="d-flex flex-row  me-5 ms-5 justify-content-between align-items-center ">
          <img src={logo} alt="logo" className="img-fluid " width="90" />

          <div className="flex-shrink-0 dropdown  ">
            <a href className="d-block link-dark text-decoration-none dropdown-toggle user-color"  id="dropdownUser2"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <i className=" fs-2 bi bi bi-person-circle "></i>
            </a>
            <ul
              className="dropdown-menu text-small shadow"
              aria-labelledby="dropdownUser2"
            >
              <li className="dropdown-item user-name">{name}</li>
              <li>
                <hr className="dropdown-divider" />
              </li>
              <li>
                <a className="dropdown-item log-out" onClick={OnClickEvent}>
                  Cerrar sesion
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
