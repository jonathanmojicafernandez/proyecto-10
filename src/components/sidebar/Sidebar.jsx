import React, { useEffect } from "react";
import "../estilos/sidebar.css";
const Sidebar = () => {
  var rol = localStorage.getItem('rol');

  return (
    <div className="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-with shadow pe-3 me-5 bg-body rounded ">
      <div className="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
        <ul
          className="nav nav-pills  flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start"
          id="menu"
        >
          <li className="nav-item">
            <a href="/Dashboard" className="nav-link align-middle px-0">
              <i className=" fs-4 bi bi-house"></i>
              <span className="ms-1 d-none d-sm-inline">Inicio</span>
            </a>
          </li>
          {rol === "3" ?
            <li>
              <a href="/Usuario" className="nav-link px-0 align-middle">
                <i class="fs-4 bi bi-people"></i>
                <span className="ms-1 d-none d-sm-inline">Usuarios</span>
              </a>
            </li>
            : null
          }
          {rol === "3" ?
            <li>
              <a href="/Laboratorio" className="nav-link px-0 align-middle">
                <i className=" fs-4 bi bi-shop-window"></i>
                <span className="ms-1 d-none d-sm-inline">Laboratorios</span>
              </a>
            </li>
            : null
          }
          <li>
            <a href="/Material" className="nav-link px-0 align-middle">
              <i class="fs-4 bi bi-grid"></i>
              <span className="ms-1 d-none d-sm-inline">Materiales</span>
            </a>
          </li>
          {/* {rol === "3" ? */}
            <li>
              <a href="/Entrada" className="nav-link px-0 align-middle">
                <i class="fs-4 bi bi-box-seam"></i>
                <span className="ms-1 d-none d-sm-inline">Ent. materiales</span>
              </a>
            </li>
            {/* : null
          } */}
          {rol === "3" ?
            <li>
              <a href="/Categoria" className="nav-link px-0 align-middle">
                <i className="fs-4 bi bi-bookmark"></i>
                <span className="ms-1 d-none d-sm-inline">Categoria</span>
              </a>
            </li>
            : null
          }
          <li>
            <a href="/Prestamo" className="nav-link px-0 align-middle">
              <i className=" fs-4 bi bi-clipboard-check"></i>
              <span className="ms-1 d-none d-sm-inline">Prestamo</span>
            </a>
          </li>
          <li>
            {rol === "2" ?
              <a
                href="#detallePrestamos"
                data-bs-toggle="collapse"
                className="nav-link px-0 align-middle "
              >
                <i className="fs-4 bi bi-clipboard"></i>
                <span className="ms-1 d-none d-sm-inline">Incumplimiento de prestamos</span>
              </a>
              : null
            }
            <ul
              className="collapse nav flex-column ms-1"
              id="detallePrestamos"
              data-bs-parent="#menu"
            >
              <li className="w-100">
                <a href="/Adeudo" className="nav-link px-0">
                  <span className="d-none d-sm-inline"><i class="bi bi-clock-history me-1"></i></span>
                  Adeudos
                </a>
              </li>
              <li>
                <a href="/Reposicion" className="nav-link px-0">
                  <span className="d-none d-sm-inline"><i class="bi bi-clipboard-x me-1"></i></span>
                  Reposiciones
                </a>
              </li>
            </ul>
          </li>
        </ul>
        <hr />
      </div>
    </div>
  );
};

export default Sidebar;
