import React, { useState, useEffect } from 'react'
import TituloSeccion from '../titulos/TituloSeccion';
import TablaAdeudos from './TablaAdeudos'
import AdeudoService from '../../services/adeudo.service';

const ContendorAdeudos = () => {
    var rol = localStorage.getItem('rol');
    if (rol === "3") {
        localStorage.clear();
        window.location.href = "/login";
    }

    const [adeudos, setAdeudos] = useState([]);
    const [cambio, setCambio] = useState(false);

    const retrieveAdeudos = async () => {
        let response = await AdeudoService.getAll().then();
        setAdeudos(response.data);
    };

    useEffect(() => {
        retrieveAdeudos();
    }, [cambio]);
    return (
        <div className="col py-3 ">
            <div className="row" id="contenedor">
                <div className="col-sm-12 col-md-12 col-lg-9">
                    <TituloSeccion nombreSeccion="Adeudos" />
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <TablaAdeudos adedudos={adeudos} setCambio={setCambio} />
                </div>
            </div>
        </div>
    )
}

export default ContendorAdeudos
