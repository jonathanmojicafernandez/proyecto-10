import React, {useState, useEffect} from 'react';

import BarraBusqueda from '../barraBusqueda/BarraBusqueda';
import Swal from "sweetalert2";
import prestamosService from '../../services/prestamos.service';

import EntradaDataService from "../../services/entrada.service";
import ModalReposicion from './ModalReposicion';

const TablaAdeudos = ({adedudos, setCambio}) => {
  
  const [adeudoS, setAdeudoS] = useState([]);
  const [prestamoSel, setPrestamoSel] = useState({});

  const seleccionPrestamo = (prestamo) => {
    setPrestamoSel(prestamo);
  };

  
  const confirmarDev =  (id,ent ) =>{
    Swal.fire({
      title: 'Entregar',
      text: 'Este adeudo ha sido devuelto correctamente, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#1976d2 "
    }).then((result) => {
      if (result.isConfirmed) {
        realizarDev(id, ent);
      }
    })
  };
  const realizarDev = async(id, ent) =>{
    let data = {
      idPrestamo: id
    };
   let response = await prestamosService.devolucion(data).then();
    if(response.status === 200){
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Se ha finalizado correctamente el prestamo',
        showConfirmButton: false,
        timer: 1500
      });
      
    }
    cambiarEstatus(ent);
  };
  const cambiarEstatus = async (ent) =>{
    let data = {
      estatus: "Disponible",
      entradas_prestamo: ent
    };
    let response = await EntradaDataService.changeStatus(data).then();
    if(response.status === 200){
    }
   limpiar();
  };
  const limpiar = ()=>{
    setPrestamoSel({});
    setCambio(true);
  };
  const handleChange = (e)=>{
    let enter = e.target.value;
    let busqueda = adeudoS.filter((item) =>
     item.usuario.matricula.toString().toLowerCase().includes(enter)||
     item.usuario.nombres.toString().toLowerCase().includes(enter)||
     item.usuario.apellido1.toString().toLowerCase().includes(enter)||
     item.usuario.apellido2.toString().toLowerCase().includes(enter) );
    if (enter === "") {
      setAdeudoS(adedudos);
    } else {
      setAdeudoS(busqueda);
    }
  }; 
  useEffect(() => {
    setAdeudoS(adedudos)
  }, [adedudos]);
    return (
        <div>
          <ModalReposicion reposicion={prestamoSel} setCambio={setCambio}/>
         <div className="card" id="container">
           <div className="card-body">
             <div className="row">
               <div className="col-sm-12 col-md-12 col-lg-8"></div>
              <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                  <input
                    placeholder="buscar"
                    type="text"
                    style={{ height: "50px" }}
                    onChange={handleChange}
                    name="buscar"
                    id="buscar"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                  </label>
                </div>
              </div>
               <div className="row">
                 <div className="col-sm-12 col-md-12 col-lg-12">
                   <table className="table table-responsive table-hover">
                     <thead>
                       <tr id="titulo" className="border-remove">
                         <th scope="col">Matricula</th>
                         <th scope="col">Solicitante</th>
                         <th scope="col">Inicio</th>
                         <th scope="col">Fin</th>
                         <th colSpan="3" scope="col">
                           Acciones
                         </th>
                       </tr>
                     </thead>
                     <tbody>
                     {adedudos &&
                       adedudos.map((adedudo) => (
                         <tr key={adedudo._id}>
                           <td>{adedudo.usuario.matricula}</td>
                           <td>{adedudo.usuario.nombres} {adedudo.usuario.apellido1} {adedudo.usuario.apellido2}</td>
                           <td>{adedudo.inicio}</td>
                           <td>{adedudo.fin}</td>
                           <td>
                            <button
                              onClick={() => {
                                confirmarDev(adedudo._id, adedudo.entradas_prestamo );
                              }}
                              className="btn btn-outline-success"
                              id=""
                            >
                             <i class="bi bi-check2-square"></i>
                            </button>
                          </td>
                          <td>
                            <button
                             className="btn btn-outline-danger"
                             onClick={() => {
                              seleccionPrestamo(adedudo);
                            }}
                            data-bs-target="#modalReposicion"
                              data-bs-toggle="modal"
                             
                            >
                             <i class="bi bi-clipboard-x"></i>
                            </button>
                          </td>
                         </tr>
                       ))}
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
    )
}

export default TablaAdeudos
