import React from 'react'
import PropTypes from 'prop-types';
import "../estilos/categoria.css";


const Input = ({ label, type,value }) => {
    return (
        <div>
            <div className="form-floating mb-3">
                <input  type={type} className="rounded-item form-control" id="floatingInput" placeholder={label} value={value}/>
                <label for="floatingInput">{label}</label>
            </div>
        </div>
    )
}

Input.propTypes = {
    label: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string
}
export default Input
