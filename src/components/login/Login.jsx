import React,{useState,useEffect,useContext} from "react";
import "../estilos/login.css";

import LoginImg from "../../res/login.svg";
import Navbar from "../sidebar/Navbar";
import Swal from 'sweetalert2'
import LoginDataService from "../../services/login.service";
import { AuthContext } from "../../context/AuthContext";
import { authTypes } from "../../types/authTypes";


const Login = () => {
  const [matricula,setMatricula] = useState("");
  const [password,setPassword] = useState("");
  const { dispatch } = useContext(AuthContext);

  useEffect(() => {
    var tokenStorage = localStorage.getItem('token');
    console.log(tokenStorage);
    if(tokenStorage != null){
      window.location.href = "/Dashboard";
    }
    
  }
  ,[]);
  const OnClickLogin = () => {

    if(matricula.length > 0 && password.length > 0)
    {
      var data = {
        matricula: matricula,
        password: password
      };
      LoginDataService.login(data)
          .then(response => {
              if(response.data.code == 200)
              {
                localStorage.setItem('token', response.data.response.token);
                localStorage.setItem('nombre', response.data.response.nombres);
                localStorage.setItem('idU', response.data.response._id);
                localStorage.setItem('rol', response.data.response.rango.id);

                //console.log(response.data.response.token);
                dispatch({ type: authTypes.login });
                window.location.href = "/Dashboard";
              }
              if(response.data.code == 400)
              {
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: response.data.response,
                })
              }
          })
          .catch(e => {
            console.log(e.data);
          });
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Debes incluir una matricula y un password',
      })
    }
    
  }

  const handleChangeMatricula = (e) => {
    setMatricula(e.target.value);
  }

  const handleChangePassword = (e) => {
    setPassword(e.target.value);
  }

  return (
    <div className="background">
    <Navbar />
    <div className="container">
      <div className="row">
        <div className="col-lx-5  col-md-5 col-sm-12">
          <div className="card shadow   rounded-item">
            <div className="p-4 card-header  rounded-header">
              <div className="card-heading text-card">Inicio de sesion</div>
            </div>
            <div className="p-lg-5 card-body">
              <h3 className="mb-4">Hola, bienvenido! 👋👋</h3>
              <p className="text-muted text-sm mb-3">
                Sistema de prestamos de materiales
              </p>
                <div className="form-floating mb-3 ">
                  <input
                    placeholder="name@example.com"
                    type="text"
                    id="matricula"
                    value={matricula}
                    onChange={handleChangeMatricula}
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="matricula">
                    Matricula
                  </label>
                </div>
                <div className="form-floating mb-5">
                  <input
                    placeholder="Password"
                    type="password"
                    id="password"
                    value={password}
                    onChange={handleChangePassword}
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="password">
                    Contraseña
                  </label>
                </div>
                <button type="submit" onClick={OnClickLogin} className="btn btn-primary btn-lg">
                  Iniciar sesion
                </button>
            </div>
            <div className=" card-footer "></div>
            
          </div>
        </div>
        <div className="col-lx-7  col-md-7 col-sm-12 text-center ">
          <img src={LoginImg} alt="Login" width="700" className="img-fluid" />
        </div>
      </div>
    </div>
    </div>
  );
};

export default Login;
