import React from "react";
import "../estilos/login.css";

import Register from "../../res/registrar.svg";
import Navbar from "../sidebar/Navbar";



const Registro = () => {
  return (
    <div className="background">
    <Navbar />
    <div className="container">
      <div className="row">
        <div className="col-lx-5  col-md-5 col-sm-12">
          <div className="card shadow   rounded-item">
            <div className="p-4 card-header  rounded-header">
              <div className="card-heading text-card">Crear cuenta</div>
            </div>
            <div className="p-lg-5 card-body">
              <h3 className="mb-4">Hola, bienvenido! 👋👋</h3>
              <p className="text-muted text-sm mb-3">
                Registrate para poder pedir materiales de laboratorio.
              </p>
              <form id="loginForm" action="/" className="">
                <div class="input-group mb-3 ">
                  <label class="input-group-text" for="inputGroupFile01">
                    <i class="far fa-address-card"></i>
                  </label>
                  <input
                    type="file"
                    class="form-control "
                    id="inputGroupFile01"
                  />
                </div>
                <div className="form-floating mb-3 ">
                  <input
                    placeholder="Nombre"
                    type="Text"
                    id="matricula"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="matricula">
                    Nombre
                  </label>
                </div>
                <div class="row g-2">
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Apellido Paterno
                      </label>
                    </div>
                  </div>
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Apellido Materno
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row g-2">
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Matricula
                      </label>
                    </div>
                  </div>
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Contraseña
                      </label>
                    </div>
                  </div>
                </div>
                <div className="form-floating mb-3 ">
                  <select
                    class="form-select rounded-item"
                    aria-label="Default select example"
                  >
                    <option selected>Seleccione carrera</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
                <div class="row g-2">
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Ciudad
                      </label>
                    </div>
                  </div>
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        CP
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row g-2">
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Colonia
                      </label>
                    </div>
                  </div>
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Calle
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row g-2">
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Numero exterior
                      </label>
                    </div>
                  </div>
                  <div class="col-md">
                    <div className="form-floating mb-3 ">
                      <input
                        placeholder="Nombre"
                        type="Text"
                        id="matricula"
                        className="form-control rounded-item"
                      />
                      <label className="form-label" for="matricula">
                        Numero interior
                      </label>
                    </div>
                  </div>
                </div>
               
                <div className="text-sm text-muted  mb-5">
                  No tengo cuenta.
                  <a href="/">Inicia sesion aqui</a>.
                </div>
                <button type="submit" className="btn btn-primary btn-lg">
                  Registrarme
                </button>
              </form>
            </div>
            <div className=" card-footer "></div>
          </div>
        </div>
        <div className="col-lx-7  col-md-7 col-sm-12 text-center ">
          <img src={Register} alt="Login" width="700" className="img-fluid" />
        </div>
      </div>
    </div>
    </div>
  );
};

export default Registro;
