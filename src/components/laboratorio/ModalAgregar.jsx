import React, { useEffect, Component, useState } from 'react'
import "../estilos/laboratorio.css";
import Input from '../input/Input';
import LaboratorioDataService from "../../services/laboratorio.service";
import Swal from 'sweetalert2';
import UsuarioDataService from "../../services/usuario.service";


const ModalAgregar = ({ setagregado }) => {

    const [data, setData] = useState({ nombre: "", localizacion: "", responsable: "" });
    const { nombre, localizacion, responsable } = data;

    const [usuarios, setUsuario] = useState([]);


    const retrieveUsuarios = async () => {
        let response = await UsuarioDataService.getAll().then();
        setUsuario(response.data)

    }

    useEffect(() => {
        retrieveUsuarios();
    }, []);

    const handleChange = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value
        });
    };

    const saveLaboratorio = async () => {
        if (nombre.length > 0 && localizacion.length > 0 && responsable.length > 0) {
            let datos = {
                nombre: data.nombre,
                localizacion: data.localizacion,
                idUsuario: data.responsable
            };
            // alert(typeof(datos.idUsuario));
            LaboratorioDataService.create(datos).then(response => {
                Swal.fire('Registrado correctamente', '', 'success');
                // console.log(response.data);
                setagregado(true)
            })
                .catch(e => {
                    console.log(e);
                    Swal.fire('Ha ocurrido un problema', e, 'info');
                });
        } else if (nombre.length == 0) {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No se ha ingresado ningun nombre",
                showConfirmButton: false,
                timer: 1500,
            });
        } else if (localizacion.length == 0) {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No se ha ingresado ninguna localización",
                showConfirmButton: false,
                timer: 1500,
            });
        } else if (responsable.length == 0) {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No se ha ingresado ningun responsable",
                showConfirmButton: false,
                timer: 1500,
            });
        }
    }

    return (

        <div>

            <div className="modal fade" id="staticBackdrop1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">Agregar laboratorio</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div>
                                    <div id="" className="col-sm-12 col-md-12 col-lg-12">
                                        <label className="floatingInput" htmlFor="localizacion">Nombre laboratorio</label>
                                        <input id="nombre"
                                            className="form-control rounded-item"
                                            onChange={
                                                handleChange}
                                            name="nombre" label="Nombre laboratorio" type="text" />
                                    </div>
                                    <div id="" className="col-sm-12 col-md-12 col-lg-12">
                                        <label className="floatingInput" htmlFor="localizacion">Localización</label>
                                        <input id="localizacion"
                                            className="form-control rounded-item"
                                            onChange={
                                                handleChange}
                                            name="localizacion" label="localizacion" type="text" />
                                    </div>
                                    <div id="" className="col-sm-12 col-md-12 col-lg-12">
                                        <label className="floatingInput" htmlFor="localizacion">Responsable</label>
                                        <select onChange={handleChange} className="form-control rounded-item" name="responsable" id="cmbResponsable">
                                            <option value="">Selecciona una opción</option>
                                            {usuarios.map((usuario) => {
                                                return (
                                                    <option key={usuario._id} value={usuario._id}>{usuario.nombres} {usuario.apellido1} {usuario.apellido2}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button onClick={saveLaboratorio} className="btn btn-success" data-bs-dismiss="modal" id="btnModificar">Agregar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default ModalAgregar

