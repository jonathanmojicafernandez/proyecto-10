import React, { useState, useEffect } from 'react'
import "../estilos/categoria.css";
import Swal from 'sweetalert2';
import laboratorioDataService from "../../services/laboratorio.service";
import UsuarioDataService from "../../services/usuario.service";

const ModalActualizar = ({ laboratorio, setagregado, responsable }) => {
  const [nombre, setNombre] = useState("");
  const [localizacion, setLocalizacion] = useState("");
  const [usuarios, setUsuario] = useState([]);
  const [idU, setIdU] = useState(0);
  const [usuarioA, setUsuarioA] = useState();
  const [estado, setestado] = useState("");
  const [sta, setState] = useState(0);



  const retrieveUsuarios = async () => {
    let response = await UsuarioDataService.getAll().then();
    setUsuario(response.data);
  }

  const changeEstatus = () => {
    if (estado === "Activo") {
      setestado("Inactivo");
      setState(1);
    } else {
      setestado("Activo");
      setState(0);
    }
  }

  const modificarLaboratorio = async () => {

    if (nombre.length > 0 && localizacion.length > 0 && idU > 0) {
      let data = {
        id: laboratorio._id,
        nombre: nombre,
        localizacion: localizacion,
        idA: idU
      };
      
      let response = await laboratorioDataService.update(data).then();
      
   
      if (response.status === 200) {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Se han guardado los cambios correctamente',
          showConfirmButton: false,
          timer: 1500
        })
      }
      setagregado(true);
    } else if (nombre.length === 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ningun nombre",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (localizacion.length === 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ninguna localización",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (idU === 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ningun responsable",
        showConfirmButton: false,
        timer: 1500,
      });
    }

  };


  const confirmarLaboratorio = async () => {
    Swal.fire({
      title: 'Modificar',
      text: 'Este laboratorio será modificado, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#1976d2 "
    }).then((result) => {
      if (result.isConfirmed) {
        modificarLaboratorio();
      }
    })

  };

  useEffect(() => {
    setNombre(laboratorio.nombre);
    setLocalizacion(laboratorio.localizacion);
    setUsuarioA(responsable);
    setIdU(responsable._id);
    setestado(laboratorio.estado);
    retrieveUsuarios();

  }, [laboratorio])
  return (
    <div>
      <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">Actualizar laboratorio</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <div className="form-floating mb-3 ">
                <input
                  placeholder="nombre de laboratorio"
                  type="text"
                  onChange={(e) => {
                    setNombre(e.target.value);
                  }}
                  id="lab"
                  className="form-control rounded-item"
                  value={nombre}

                />
                <label className="form-label" for="lab">
                  Nombre de laboratorio
                </label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Localizacion"
                  type="text"
                  id="local"
                  onChange={(e) => {
                    setLocalizacion(e.target.value);
                  }}
                  className="form-control rounded-item"
                  value={localizacion}

                />

                <label className="form-label" for="local">
                  Localizacion
                </label>

              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Localizacion"
                  type="text"
                  id="local"
                  className="form-control rounded-item"
                  value={responsable.nombres + " " + responsable.apellido1 + " " + responsable.apellido2}
                  disabled
                />

                <label className="form-label" for="local">
                  Responsable actual
                </label>

              </div>
              <div class="form-floating">
                <select onChange={(e) => {
                  setIdU(parseInt(e.target.value));


                }} class="form-select" id="floatingSelect" aria-label="Floating label select example">
                  <option selected>Seleccione nuevo responsable</option>
                  {usuarios.map((usuario) => (

                    <option key={usuario._id} value={usuario._id}>{usuario.nombres} {usuario.apellido1} {usuario.apellido2}</option>

                  ))}

                </select>
                <label for="floatingSelect">Seleccione nuevo responsable</label>
              </div>
              <br />
              {laboratorio.estatus === "Activo" ? null :
                <div id="switch" className="form-check form-switch">
                  <input className="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked={estado === "Inactivo" ? "true" : ""} onChange={changeEstatus} />
                  <label className="form-check-label" for="flexSwitchCheckChecked">Laboratorio habilitada</label>
                </div>}


            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button onClick={confirmarLaboratorio} className="btn btn-warning" id="btnModificar" data-bs-dismiss="modal">Modificar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ModalActualizar

