import React, { useState,useEffect } from "react";
import BarraBusqueda from "../barraBusqueda/BarraBusqueda";
import "../estilos/categoria.css";
import "../estilos/tablas.css";
import Swal from "sweetalert2";
import laboratorioDataService from "../../services/laboratorio.service";

import ModalActualizar from "./ModalActualizar";

const ContainerTabla = ({ laboratorios, funcion, setagregado }) => {
  const [modalcheck, setModalCheck] = useState({});
  const [responableA, setResponsableA] = useState({});
  const [laboratorioS, setLaboratorioS] = useState([]);
  const [display, setdisplay] = useState(false);

  const confirmarEliminar = (id) => {
    Swal.fire({
      title: "Eliminar",
      text: "Este laboratorio será eliminada, ¿desea continuar?",
      icon: "warning",
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
      confirmButtonColor: "#b71c1c ",
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        deleteLaboratorio(id);
      }
    });
  };

  const deleteLaboratorio = async (id) => {
    let data = {
      id: id,
    };
    let response = await laboratorioDataService.delete(data).then();
    if (response.status === 200) {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Se ha inhabilitado correctamente",
        showConfirmButton: false,
        timer: 1500,
      });
    }
    setagregado(true);
  };

  const handleChange = (e) => {
    let enter = e.target.value;
    let busqueda = laboratorioS.filter((item) =>
     item.nombre.toString().toLowerCase().includes(enter) || item.localizacion.toString().toLowerCase().includes(enter) ||
     item.usuario.nombres.toString().toLowerCase().includes(enter)||
     item.usuario.apellido1.toString().toLowerCase().includes(enter)||
     item.usuario.apellido2.toString().toLowerCase().includes(enter));
    if (enter === "") {
      setLaboratorioS(laboratorios);
    } else {
      setLaboratorioS(busqueda);
    }
  };
  
  useEffect(() => {
    setLaboratorioS(laboratorios)
  }, [laboratorios]);

  return (
    <div>
      <ModalActualizar
        laboratorio={modalcheck}
        setagregado={setagregado}
        responsable={responableA}
      />
      <div className="card" id="container">
        <div className="card-body">
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-8"></div>
            <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                <input
                  placeholder="buscar"
                  type="text"
                  style={{ height: "50px" }}
                  onChange={handleChange}
                  name="buscar"
                  id="buscar"
                  className="form-control rounded-item"
                />
                <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                </label>
              </div>
            </div>
            <div className="row  table-responsive table_chiquita">
              <div className="col-sm-12 col-md-12 col-lg-12  table-responsive">
                <table className="table table-hover">
                  <thead>
                    <tr id="titulo" className="border-remove">
                      <th scope="col">Nombre Laboratorio</th>
                      <th scope="col">Localización</th>
                      <th scope="col">Responsable</th>
                      <th colSpan="2" scope="col">
                        Acciones
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {laboratorioS &&
                      laboratorioS.map((laboratorio) => (
                        <tr key={laboratorio._id}>
                          <td>{laboratorio.nombre}</td>
                          <td>{laboratorio.localizacion}</td>
                          <td>
                            {laboratorio.usuario.nombres}{" "}
                            {laboratorio.usuario.apellido1}{" "}
                            {laboratorio.usuario.apellido2}
                          </td>
                          <td>
                            <button
                              onClick={() => confirmarEliminar(laboratorio._id)}
                              className="btn btn-outline-danger"
                              id=""
                            >
                              <i className="bi bi-trash"></i>
                            </button>
                          </td>
                          <td>
                            <button
                              onClick={() => {
                                setModalCheck(laboratorio);
                                setResponsableA(laboratorio.usuario);
                              }}
                              data-bs-target="#staticBackdrop"
                              className="btn btn-outline-warning"
                              data-bs-toggle="modal"
                              data-bs-target="#staticBackdrop"
                            >
                              <i className="bi bi-pencil-square"></i>
                            </button>
                          </td>
                          
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContainerTabla;
