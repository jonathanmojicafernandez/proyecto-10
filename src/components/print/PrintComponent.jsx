import React ,{ useRef } from 'react';
import ReactToPrint from 'react-to-print';
import ImprimirClave from '../entrada/ImprimirClave';
import '../estilos/button.css';

const PrintComponent = ({clave}) => {
    let componentRef = useRef();
    return (
        <div>
            
            <ReactToPrint 
            trigger={() => <button  className="btn btn" id="btn-secondary"><i class="bi bi-printer"></i> Claves</button> }
            content={() => componentRef}
            />
            <div style={{display:"none" }}>
            <ImprimirClave clave={clave}   ref={(el) => (componentRef = el)} />
            </div>
          
        </div>
    )
}

export default PrintComponent
