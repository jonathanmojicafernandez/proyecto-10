import React from 'react';
import "../estilos/categoria.css";

const BarraBusqueda = () => {
    return (
        <div>
            <div className="row">
                <div className="input-group mb-3" id="barra">
                    <input type="text" className="rounded-item form-control" placeholder="Buscar" aria-label="Recipient's username" aria-describedby="button-addon2" />
                </div>
            </div>
        </div>
    )
}

export default BarraBusqueda
