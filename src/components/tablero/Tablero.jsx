import React from "react";
import Header from "../sidebar/Header";
import Sidebar from "../sidebar/Sidebar";
import ContenedorCategoria from "../categoria/ContenedorCategoria";
import ContenedorLaboratorio from "../laboratorio/ContenedorLaboratorio";
import ContenedorUsuario from "../usuario/ContenedorUsuario";
import { Route, Switch, Redirect } from "react-router";
import ContenedorMaterial from "../material/ContenedorMaterial";
import ContenedorPrestamo from "../prestamo/ContenedorPrestamo";
import ContenedorEntrada from "../entrada/ContenedorEntrada";
import ContendorAdeudos from "../adeudo/ContendorAdeudos";
import Dashboard from "../dashboard/Dashboard";
import ContenedorReposicion from "../reposicion/ContenedorReposicion";

const Tablero = () => {
  return (
    <div className="bg-light">
      <Header />
      <div className="container-fluid ">
        <div className="row flex-nowrap">
          <Sidebar />
           <Switch>
             {/* <Redirect to="/Dashboard" /> */}
             <Route exact path="/Categoria" component={ContenedorCategoria} />
             <Route exact path="/Dashboard" component={Dashboard} />
             <Route exact path="/Laboratorio" component={ContenedorLaboratorio} />
             <Route exact path="/Material" component={ContenedorMaterial} /> 
             <Route exact path="/Prestamo" component={ContenedorPrestamo} /> 
             <Route exact path="/Entrada" component={ContenedorEntrada} /> 
             <Route exact path="/Adeudo" component={ContendorAdeudos} /> 
             <Route exact path="/Reposicion" component={ContenedorReposicion} /> 
             <Route exact path="/Usuario" component={ContenedorUsuario} />
           </Switch>
          {/* <Contenedor /> */}
        
        </div>
      </div>
    </div>
  );
};

export default Tablero;
