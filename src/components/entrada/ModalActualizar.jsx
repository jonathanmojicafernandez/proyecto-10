import React, {useEffect, useState} from 'react';
import EntradaDataService from '../../services/entrada.service';

import Swal from 'sweetalert2';

const ModalActualizar = ({entrada, setCambio}) => {
    const [costo, setCosto] = useState(0);
    const [entrad, setEntrad] = useState({ laboratorio: 0 , material: 0 , id: 0, clave: "" });

    const confirmarUpdate = () =>{
      if(costo !== '' && costo !== "0"){
        Swal.fire({
            title: 'Modificar',
            text: 'Este material será modificado, ¿desea continuar?',
            icon: 'warning',
            showCancelButton: true,
            closeOnConfirm: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            confirmButtonColor: "#1976d2 "
          }).then((result) => {
            if (result.isConfirmed) {
                updateEntrada();
            }
          })
        }else{
          Swal.fire({
            position: "center",
            icon: "info",
            title: "No todos los campos estan llenos",
            showConfirmButton: false,
            timer: 1500
          });
          setCosto(entrada.costo);
        }

    };

    const updateEntrada = async () =>{
        let data = {
          _id: entrad.id,
          costo: parseFloat(costo)
      };
      let response = await EntradaDataService.update(data).then();
      if(response.status === 200){
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Se han guardado los cambios correctamente',
            showConfirmButton: false,
            timer: 1500
          })
        }
        setCambio(true);

      
        
    };

    useEffect(() => {
        if (entrada._id != null || entrada._id !== undefined) {
            setCosto(entrada.costo);
            setEntrad({
                laboratorio: entrada.laboratorio,
                material:entrada.material,
                costo: entrada.costo,
                id: entrada._id,
                clave: entrada.clave
            });
        } else {
          console.log("No hay datos en el producto");
        }
      }, [entrada]);
    return (
        <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="staticBackdropLabel">Actualizar material</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <div className="form-floating mb-3 ">
              <input
                placeholder="nombre de laboratorio"
                type="text"
                id="lab"
                className="form-control rounded-item"
                value={entrad.clave}
                disabled
                
              />
              <label className="form-label" for="lab">
                Clave del material
              </label>
            </div>
                <div className="form-floating mb-3 ">
              <input
                placeholder="nombre de laboratorio"
                type="text"
                id="lab"
                className="form-control rounded-item"
                value={entrad.material.nombre}
                disabled
              />
              <label className="form-label" for="lab">
                Nombre del material
              </label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Laboratorio"
                type="text"
                id="local"
                className="form-control rounded-item"
                value={entrad.laboratorio.nombre}
                disabled
              />
              
              <label className="form-label" for="local">
                Laboratorio
              </label>
             
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Localizacion"
                type="text"
                id="local"
                className="form-control rounded-item"
                onKeyPress={(evt) => { if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57){evt.preventDefault(); }}}
                value={costo}
                onChange={(e) => {
                    setCosto(e.target.value);
                  }}
              />
              
              <label className="form-label" for="local">
                Costo $
              </label>
             
            </div>
           
            <br />
                </div>
                <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button onClick={confirmarUpdate} className="btn btn-warning" id="btnModificar" data-bs-dismiss="modal">Modificar</button>
                </div>
            </div>
        </div>
    </div>
    )
}

export default ModalActualizar
