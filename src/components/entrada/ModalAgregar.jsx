import React, { useEffect, useState } from "react";
import MaterialDataService from "../../services/material.service";
import laboratorioDataService from "../../services/laboratorio.service";
import EntradaDataService from "../../services/entrada.service";

import Swal from "sweetalert2";

const ModalAgregar = ({ setCambio, setClave }) => {
  const [materiales, setMateriales] = useState([]);
  const [laboratorios, setLaboratorios] = useState([]);
  const [entrada, setEntrada] = useState({
    laboratorio: 0,
    material: 0,
    costo: 0,
    cantidad: 0,
  });

  const allLab = async () => {
    let response = await laboratorioDataService.getAll().then();
    setLaboratorios(response.data);
  };
  const handleChange = (e) => {
      setEntrada({
        ...entrada,
        [e.target.name]: e.target.value,
      });
    
  };
  const allMat = async () => {
    let response = await MaterialDataService.getAll().then();
    setMateriales(response.data);
  };
  const agregarEntrada = async () => {
    if (entrada.laboratorio !== 0 && entrada.material !== 0 && entrada.costo !== 0 && entrada.cantidad !== 0) {
      let data = {
        laboratorioId: parseInt(entrada.laboratorio),
        materialId: parseInt(entrada.material),
        cantidad: parseInt(entrada.cantidad),
        costo: parseFloat(entrada.costo),
      };
      let response = await EntradaDataService.create(data).then();
      if (response.status === 200) {
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Se han guardado los cambios correctamente",
          confirmButtonText: "Save",
          denyButtonText: `Don't save`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            mostrarClaves();
          } else if (result.isDenied) {
          }
        });
      }
     
    }else{
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No todos los campos estan llenos",
        showConfirmButton: false,
        timer: 1500
      });
      return ;
    }
    
  };

  const mostrarClaves = async () => {
    let data = parseInt(entrada.cantidad);
    let response = await EntradaDataService.getClave(data);
    setClave(response.data);
    setCambio(true);
  };

  useEffect(() => {
    allMat();
    allLab();
  }, []);

  return (
    <div
      className="modal fade"
      id="staticBackdrop1"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabindex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Agregar entradas de materiales
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body ">
            <div className="row">
              <div className="col">
                <div class="form-floating">
                  <select
                    onChange={handleChange}
                    class="form-select rounded-item"
                    name="laboratorio"
                    id="labselect"
                    aria-label="Floating label select example"
                  >
                    <option selected>Selecciona el laboratorio</option>
                    {laboratorios.map((lab) => (
                      <option key={lab._id} value={lab._id}>
                        {lab.nombre}
                      </option>
                    ))}
                  </select>
                  <label for="labselect">Laboratorios</label>
                </div>
              </div>
            </div>

            <div className="row mt-3">
              <div className="col">
                <div class="form-floating">
                  <select
                    class="form-select rounded-item"
                  
                    onChange={handleChange}
                    name="material"
                    id="matSelect"
                    aria-label="Floating label select example"
                  >
                    <option selected>Selecciona el material</option>
                    {materiales.map((mat) => (
                      <option key={mat._id} value={mat._id}>
                        {mat.nombre}
                      </option>
                    ))}
                  </select>
                  <label for="matSelect">Material</label>
                </div>
              </div>
            </div>
            <div class="row mt-3">
              <div className="col ">
                <div className="form-floating mb-3 input-group-sm">
                  <input
                    placeholder="Cantidad"
                    type="number"
                    style={{ height: "50px" }}
                    min="1"
                    pattern="^[a-zA-Z0-9]+$-"
                    onKeyPress={(evt) => { if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57){evt.preventDefault(); }}}
                    onChange={handleChange}
                    name="cantidad"
                    id="matricula"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="lab">
                    Cantidad
                  </label>
                </div>
              </div>
              <div className="col">
                <div className="form-floating mb-3  input-group-sm">
                  <input
                    placeholder="Cantidad"
                    type="number"
                    style={{ height: "50px" }}
                    min="1"
                    name="costo"
                    pattern="^[0-9]+"
                    onKeyPress={(evt) => { if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57){evt.preventDefault(); }}}
                    onChange={handleChange}
                    id="matricula"
                    className="form-control rounded-item"
                  />
                  <label className="form-label" for="lab">
                    Costo $
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Cancelar
            </button>
            <button
              onClick={agregarEntrada}
              className="btn btn-success"
              data-bs-dismiss="modal"
              id="btnModificar"
            >
              Agregar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalAgregar;
