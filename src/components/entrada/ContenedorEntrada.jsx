import React, { useState, useEffect } from "react";
import PrintComponent from "../print/PrintComponent";
import TituloSeccion from "../titulos/TituloSeccion";
import ModalAgregar from "./ModalAgregar";
import TablaEntradas from "./TablaEntradas";

import EntradaDataService from "../../services/entrada.service";

const ContenedorEntrada = () => {
  var rol = localStorage.getItem('rol');
  // if (rol === "2") {
  //   localStorage.clear();
  //   window.location.href = "/login";
  // }

  const [cambio, setCambio] = useState(false);
  const [clave, setClave] = useState("");
  const [entradas, setEntradas] = useState([]);

  const retrieveEntrada = async () => {
    let response = await EntradaDataService.getAll().then();
    setEntradas(response.data);
  };

  useEffect(() => {
    retrieveEntrada();
    setCambio(false);
  }, [cambio]);
  return (
    <div className="col py-3 ">
      <div className="row" id="contenedor">
        <div className="col-sm-12 col-md-12 col-lg-7">
          <TituloSeccion nombreSeccion="Entradas" />
        </div>
        <div className="col-sm-12 col-md-12 col-lg-2 ">
          {clave === "" ? null : <PrintComponent clave={clave} />}
        </div>
        <div className="col-sm-12 col-md-12 col-lg-3 mb-3">
          {rol === "3" ?
            <button
              data-bs-toggle="modal"
              data-bs-target="#staticBackdrop1"
              className="btn btn"
              id="btnAgregar"
            >
              <i className="bi bi-plus-lg"></i> Agregar Nuevo
            </button>
            : null
          }
        </div>
      </div>
      <div className="row">
        <div className="col-sm-12 col-md-12 col-lg-12">
          <TablaEntradas entradas={entradas} setCambio={setCambio} />
        </div>
        <ModalAgregar setCambio={setCambio} setClave={setClave} />
      </div>
    </div>
  );
};

export default ContenedorEntrada;
