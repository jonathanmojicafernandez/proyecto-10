import React, { useState, useEffect } from "react";

import Swal from "sweetalert2";
import EntradaDataService from "../../services/entrada.service";
import ModalActualizar from "./ModalActualizar";

const TablaEntradas = ({ entradas, setCambio }) => {
  var rol = localStorage.getItem('rol');
  const [entrada, setEntrada] = useState({});
  const [entradasS, setEntradasS] = useState([]);

  const entradaSelect = (entrada) => {
    setEntrada(entrada);
  };

  const confirmarEliminar = (id) => {
    Swal.fire({
      title: "Eliminar",
      text: "Este material será eliminada, ¿desea continuar?",
      icon: "warning",
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
      confirmButtonColor: "#b71c1c ",
    }).then((result) => {
      if (result.isConfirmed) {
        deleteEntrada(id);
      }
    });
  };

  const deleteEntrada = async (id) => {
    let data = {
      _id: id,
    };
    let response = await EntradaDataService.delete(data).then();
    if (response.status === 200) {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Se ha inhabilitado correctamente",
        showConfirmButton: false,
        timer: 1500,
      });
    }
    setCambio(true);
  };

  const handleChange = (e) => {
    let enter = e.target.value;
    let busqueda = entradasS.filter((item) =>
      item.clave.toString().toLowerCase().includes(enter) || item.material.nombre.toString().toLowerCase().includes(enter) ||
      item.laboratorio.nombre.toString().toLowerCase().includes(enter) || item.costo.toString().toLowerCase().includes(enter));

    if (enter === "") {
      setEntradasS(entradas);
    } else {
      setEntradasS(busqueda);
    }

  };

  useEffect(() => {
    setEntradasS(entradas)
  }, [entradas]);

  return (
    <div>
      <ModalActualizar entrada={entrada} setCambio={setCambio} />
      <div className="card" id="container">
        <div className="card-body">
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-8"></div>
            <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                <input
                  placeholder="buscar"
                  type="text"
                  style={{ height: "50px" }}
                  onChange={handleChange}
                  name="buscar"
                  id="buscar"
                  className="form-control rounded-item"
                />
                <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                </label>
              </div>
            </div>
            <div className="row  table-responsive table_chiquita">
              <div className="col-sm-12 col-md-12 col-lg-12  table-responsive">
                <table className="table table-hover">
                  <thead className="thead_sticky">
                    <tr id="titulo" className="border-remove">
                      <th scope="col">Cable</th>
                      <th scope="col">Material</th>
                      <th scope="col">Laboratorio</th>
                      <th scope="col">Costo</th>
                      {rol === "3" ?
                        <th colSpan="2" scope="col">
                          Acciones
                        </th>
                        : null
                      }
                    </tr>
                  </thead>
                  <tbody>
                    {entradasS &&
                      entradasS.map((entrada) => (
                        <tr key={entrada._id}>
                          <td>{entrada.clave}</td>
                          <td>{entrada.material.nombre}</td>
                          <td>{entrada.laboratorio.nombre}</td>
                          <td>${entrada.costo}</td>
                          {rol === "3" ?
                            <td>
                              <button
                                onClick={() => {
                                  confirmarEliminar(entrada._id);
                                }}
                                className="btn btn-outline-danger"
                                id=""
                              >
                                <i className="bi bi-trash"></i>
                              </button>
                            </td>
                            : null
                          }
                          {rol === "3" ?
                            <td>
                              <button
                                onClick={() => {
                                  entradaSelect(entrada);
                                }}
                                data-bs-target="#staticBackdrop"
                                className="btn btn-outline-warning"
                                data-bs-toggle="modal"
                                data-bs-target="#staticBackdrop"
                              >
                                <i className="bi bi-pencil-square"></i>
                              </button>
                            </td>
                            : null
                          }
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TablaEntradas;
