
import React, { useEffect, useState } from 'react'
import TituloSeccion from "../titulos/TituloSeccion";
import "../estilos/categoria.css";
import ContainerTabla from './ContainerTabla';
import ModalActualizar from './ModalActualizar';
import MaterialDataService from "../../services/material.service";
import ModalAgregar from './ModalAgregar';

const ContenedorMaterial = () => {
    var rol = localStorage.getItem('rol');

    const [material, setmaterial] = useState([]);
    const [categorias, setcategorias] = useState([]);
    const [agregado, setagregado] = useState(false)

    const retrievemateriales = async () => {
        MaterialDataService.getAll()
            .then(response => {
                setmaterial(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    useEffect(() => {
        retrievemateriales()
        setagregado(false)
    }, [agregado])



    return (
        <div className="col py-3 ">
            <div className="row" id="contenedor">
                <div className="col-sm-12 col-md-12 col-lg-9">
                    <TituloSeccion nombreSeccion="Material" />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-3 mb-3">
                    {rol === "3" ?
                        <button data-bs-toggle="modal" data-bs-target="#staticBackdrop1" className="btn btn" id="btnAgregar"><i className="bi bi-plus-lg"></i>Agregar Nueva</button>
                        : null
                    }
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <ContainerTabla materiales={material} funcion={retrievemateriales} setagregado={setagregado} />
                </div>
                <div id="" className="col-sm-12 col-md-12 col-lg-12">
                    <ModalAgregar setagregado={setagregado} />

                </div>
            </div>
        </div>
    );
};

export default ContenedorMaterial
