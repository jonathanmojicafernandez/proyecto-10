import React, { useState, useEffect } from 'react'
import "../estilos/categoria.css";
import Swal from 'sweetalert2';
import MaterialDataService from "../../services/material.service";
import CategoriaDataService from "../../services/categoria.service";

const ModalActualizar = ({ material, setagregado, categorias }) => {
  const [nombre, setNombre] = useState("");
  const [descripcion, setdescripcion] = useState("");
  const [categoria, setCategoria] = useState([]);
  const [idC, setidC] = useState(0);
  const [categoriaA, setCategoriaA] = useState();
  const [estado, setestado] = useState("");
  const [sta, setState] = useState(0);

  const [fileInputState, setFileInputState] = useState("");
  const [selectedFile, setSelectedFile] = useState("");
  const [previewSource, setPreviewSource] = useState()
  const [foto, setFoto] = useState("");


  const retrievecategoria = async () => {
    let response = await CategoriaDataService.getAll().then();
    setCategoria(response.data);
  }

  const changeEstatus = () => {
    if (estado === "Activo") {
      setestado("Inactivo");
      setState(1);
    } else {
      setestado("Activo");
      setState(0);
    }
  }

  const modificarMaterial = async () => {

    if (nombre.length > 0 && descripcion.length > 0 && categoria.length > 0) {
      let data = {
        id: material._id,
        nombre: nombre,
        descripcion: descripcion,
        idA: idC,
        foto:foto,
        estatus: parseInt(sta)
      };
      console.log("data");
      console.log(data);
      let response = await MaterialDataService.update(data).then();

      if (response.status === 200) {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Se han guardado los cambios correctamente',
          showConfirmButton: false,
          timer: 1500
        })
      }
      setagregado(true);
    } else if (nombre.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ningun nombre",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (descripcion.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ninguna descripción",
        showConfirmButton: false,
        timer: 1500,
      });
    } else if (categoria.length == 0) {
      Swal.fire({
        position: "center",
        icon: "info",
        title: "No se ha ingresado ninguna categoria",
        showConfirmButton: false,
        timer: 1500,
      });
    }

  };


  const confirmarmaterial = async () => {
    Swal.fire({
      title: 'Modificar',
      text: 'Este material será modificado, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#1976d2 "
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        modificarMaterial();
      }
    })

  };

  useEffect(() => {
    setNombre(material.nombre);
    setdescripcion(material.descripcion);
    setCategoriaA(categoria);
    setidC(categorias._id);
    setestado(material.estado);
    setFoto(material.foto);
    retrievecategoria();

  }, [material])

  const uploadImage = async (base64EncodedImage) => {
    // alert(base64EncodedImage);
    setFoto(base64EncodedImage);
  }

  const handleFileInputChange = (e) => {
    const file = e.target.files[0];
    previewFile(file);
  }

  const previewFile = (file) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setFoto(reader.result);
    }
  }

  return (
    <div>
      <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">Actualizar material</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <div className="form-floating mb-3 ">
                <input
                  placeholder="nombre de material"
                  type="text"
                  onChange={(e) => {
                    setNombre(e.target.value);
                  }}
                  id="lab"
                  className="form-control rounded-item"
                  value={nombre}

                />
                <label className="form-label" for="lab">
                  Nombre de material
                </label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="descripcion"
                  type="text"
                  id="local"
                  onChange={(e) => {
                    setdescripcion(e.target.value);
                  }}
                  className="form-control rounded-item"
                  value={descripcion}

                />

                <label className="form-label" for="local">
                  descripcion
                </label>

              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="descripcion"
                  type="text"
                  id="local"
                  className="form-control rounded-item"
                  value={categorias.nombre}
                  disabled
                />

                <label className="form-label" for="local">
                  categoria actual
                </label>

              </div>
              <div class="form-floating">
                <select onChange={(e) => {
                  setidC(parseInt(e.target.value));


                }} class="form-select" id="floatingSelect" aria-label="Floating label select example">
                  <option selected>Seleccione nueva categoria</option>
                  {categoria.map((categoria) => (

                    <option key={categoria._id} value={categoria._id}>{categoria.nombre} </option>

                  ))}

                </select>
                <label for="floatingSelect">Seleccione nuevo categoria</label>
              </div>
              <div id="fotoMat" className="col-sm-12 col-md-12 col-lg-12">
                <div className="form-floating mb-3">
                  <input
                    placeholder="fotografía"
                    name="image"
                    type="file"
                    id="local"
                    onChange={handleFileInputChange}
                    className="form-control rounded-item"
                    value={fileInputState}

                  />

                  <label className="form-label" for="local">
                    Fotografía(opcional)
                  </label>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                  {
                    foto && (
                      <img src={foto} alt="chosen" style={{ height: '100%', width: '100%' }} />
                    )
                  }
                </div>

              </div>
              <br />
              {material.estatus === "Activo" ? null :
                <div id="switch" className="form-check form-switch">
                  <input className="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked={estado === "Inactivo" ? "true" : ""} onChange={changeEstatus} />
                  <label className="form-check-label" for="flexSwitchCheckChecked">material habilitada</label>
                </div>}


            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button onClick={confirmarmaterial} className="btn btn-warning" id="btnModificar" data-bs-dismiss="modal">Modificar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ModalActualizar

