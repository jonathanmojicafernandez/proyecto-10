import React, { Component, useState, useEffect } from 'react'
import "../estilos/categoria.css";
import Input from '../input/Input';
import CategoriaDataService from "../../services/categoria.service";
import MaterialDataService from "../../services/material.service";
import Swal from 'sweetalert2';


const ModalAgregar = ({ setagregado }) => {

    const [nombre, setnombre] = useState("")
    const [descripcion, setdescripcion] = useState("")
    const [categoria, setcategoria] = useState("")
    const [categorias, setcategorias] = useState([])

    const [selectedFile, setSelectedFile] = useState("");
    const [previewSource, setPreviewSource] = useState()
    const [foto, setFoto] = useState("");
    const [fileInputState, setFileInputState] = useState("");

    const onChangeNombre = (e) => {
        setnombre(e);
    }

    const onChangeDescripcion = (e) => {
        setdescripcion(e);
    }

    const onChangeCategoria = (e) => {
        let index = e.target.selectedIndex;
        console.log(e.target.options[index].value);
        setcategoria(e.target.options[index].value);
    }

    const retrieveCategorias = async () => {
        CategoriaDataService.getAll()
            .then(response => {
                setcategorias(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }
    useEffect(() => {
        retrieveCategorias();
    }, []);

    const saveMaterial = async () => {
        if (nombre.length > 0 && descripcion.length > 0 && categoria.length > 0) {
            let data = {
                nombre: nombre,
                descripcion: descripcion,
                foto:foto,
                categoria: categoria,
            };
            console.log(data);
            MaterialDataService.create(data).then();
            Swal.fire({
                position: "center",
                icon: "success",
                title: "Se registrado correctamente el material",
                showConfirmButton: false,
                timer: 1500,
            });
            setagregado(true)
        } else if (nombre.length == 0 ) {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No se ha ingresado ningun nombre",
                showConfirmButton: false,
                timer: 1500,
            });
        }else if (descripcion.length == 0 ) {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No se ha ingresado ninguna descripción",
                showConfirmButton: false,
                timer: 1500,
            });
        }else if (categoria.length == 0 ) {
            Swal.fire({
                position: "center",
                icon: "info",
                title: "No se ha ingresado ninguna categoria",
                showConfirmButton: false,
                timer: 1500,
            });
        }
    }

    const uploadImage = async (base64EncodedImage) => {
        // alert(base64EncodedImage);
        setFoto(base64EncodedImage);
    }

    const handleFileInputChange = (e) => {
        const file = e.target.files[0];
        previewFile(file);
    }

    const previewFile = (file) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
            setFoto(reader.result);
        }
    }

    return (

        <div>

            <div className="modal fade" id="staticBackdrop1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">Agregar material</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-sm-4 col-md-4 col-lg-4">
                                    <label className="floatingInput" htmlFor="localizacion">Nombre</label>
                                    <input id="nombre"
                                        className="form-control rounded-item"
                                        onChange={(e) => {
                                            onChangeNombre(e.target.value);
                                        }}
                                        name="nombre" label="Nombre Material" type="text" />
                                </div>
                                <div className="col-sm-4 col-md-4 col-lg-4">
                                    <label className="floatingInput" htmlFor="localizacion">Descripción</label>
                                    <input id="descripcion"
                                        className="form-control rounded-item"
                                        onChange={(e) => {
                                            onChangeDescripcion(e.target.value);
                                        }}
                                        name="descripcion" label="Descripción" type="text" />
                                </div>
                                <div className="col-sm-4 col-md-4 col-lg-4">
                                    <label className="floatingInput" htmlFor="localizacion">Categoria</label>
                                    <select onChange={onChangeCategoria} className="form-control rounded-item" name="cmbResponsable" id="cmbResponsable">
                                        <option value="">Selecciona una opción</option>
                                        {categorias.map((cate) => {
                                            return (
                                                <option key={cate._id} value={cate._id}>{cate.nombre} </option>
                                            );
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div id="fotoMat" className="col-sm-12 col-md-12 col-lg-12">
                                <div className="form-floating mb-3">
                                    <input
                                        placeholder="fotografía"
                                        name="image"
                                        type="file"
                                        id="local"
                                        onChange={handleFileInputChange}
                                        className="form-control rounded-item"
                                        value={fileInputState}

                                    />

                                    <label className="form-label" for="local">
                                        Fotografía
                                    </label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12 col-md-12 col-lg-12">
                                    {
                                        foto && (
                                            <img src={foto} alt="chosen" style={{ height: '100%', width: '100%' }} />
                                        )
                                    }
                                </div>

                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button onClick={saveMaterial} className="btn btn-success" data-bs-dismiss="modal" id="btnModificar">Agregar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default ModalAgregar

