import React, { Component, useState, useEffect } from 'react'
import BarraBusqueda from '../barraBusqueda/BarraBusqueda';
import "../estilos/categoria.css";
import Swal from 'sweetalert2';
import MaterialDataService from "../../services/material.service";

import ModalActualizar from './ModalActualizar';
var rol = localStorage.getItem('rol');
const ContainerTabla = ({ materiales, funcion, setagregado }) => {
  const [modalcheck, setModalCheck] = useState({});
  const [materialS, setMaterialS] = useState([]);
  const [categoriaA, setcategoriaA] = useState({});

  const confirmarEliminar = (id) => {
    Swal.fire({
      title: 'Eliminar',
      text: 'Este material será eliminado, ¿desea continuar?',
      icon: 'warning',
      showCancelButton: true,
      closeOnConfirm: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: "#b71c1c "
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        deletematerial(id);
      }
    })
  };

  const deletematerial = async (id) => {
    let data = {
      id: id,
    };
    let response = await MaterialDataService.delete(data).then();
    if (response.status === 200) {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Se ha inhabilitado correctamente",
        showConfirmButton: false,
        timer: 1500,
      });
    }
    setagregado(true);
  }

  const handleChange = (e) => {
    let enter = e.target.value;
    let busqueda = materialS.filter((item) =>
      item.nombre.toString().toLowerCase().includes(enter) || item.descripcion.toString().toLowerCase().includes(enter) ||
      item.categoria.nombre.toString().toLowerCase().includes(enter));

    if (enter === "") {
      setMaterialS(materiales);
    } else {
      setMaterialS(busqueda);
    }
  };

  useEffect(() => {
    setMaterialS(materiales)
  }, [materiales]);

  return (

    <div>
      <ModalActualizar
        material={modalcheck}
        setagregado={setagregado}
        categorias={categoriaA} />
      <div className="card" id="container">

        <div className="card-body">
          <div className="row">
            <div className="col-sm-12 col-md-12 col-lg-8"></div>
            <div className="col-sm-12 col-md-12 col-lg-4">
              <div className="form-floating mb-3 me-5 input-group-sm">
                <input
                  placeholder="buscar"
                  type="text"
                  style={{ height: "50px" }}
                  onChange={handleChange}
                  name="buscar"
                  id="buscar"
                  className="form-control rounded-item"
                />
                <label className="form-label" for="lab">
                  <i class="bi bi-search"></i> Buscar
                </label>
              </div>
            </div>
            <div className="row  table-responsive table_chiquita">
              <div className="col-sm-12 col-md-12 col-lg-12  table-responsive">
                <table className="table table-hover">
                  <thead className="thead-light">
                    <tr id="titulo" className="border-remove">
                      <th scope="col">Nombre Material</th>
                      <th scope="col">Imagen</th>
                      <th scope="col">Descripción</th>
                      <th scope="col">Estatus</th>
                      <th scope="col">Categoria</th>
                      {rol === "3" ?
                        <th colSpan="2" scope="col">Acciones</th>
                        : null
                      }
                    </tr>
                  </thead>
                  <tbody>
                    {materialS &&
                      materialS.map((material) => (
                        <tr key={material._id}>
                          <td  >
                            {material.nombre}</td>
                          <td  >
                            <img src={material.foto} alt="" style={{ height: '30%', width: '30%' }} /></td>
                          <td>
                            {material.descripcion}</td>
                          <td>
                            {material.estatus}</td>
                          <td>
                            {material.categoria.nombre}</td>

                          {rol === "3" ?
                            <td><button onClick={() => confirmarEliminar(material._id)} className="btn btn-outline-danger" id=""><i className="bi bi-trash"></i></button></td>
                            : null
                          }

                          {rol === "3" ?
                            <td><button onClick={() => {
                              console.log(material);
                              setModalCheck(material);
                              setcategoriaA(material.categoria);
                            }} data-bs-target="#staticBackdrop" className="btn btn-outline-warning" data-bs-toggle="modal" data-bs-target="#staticBackdrop"><i className="bi bi-pencil-square"></i></button></td>
                            : null
                          }
                        </tr>

                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>


  )
}

export default ContainerTabla



