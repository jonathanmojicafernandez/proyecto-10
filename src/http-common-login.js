import axios from "axios";

export default axios.create({
    //baseURL: "http://localhost:8080/api",
    baseURL: "https://secret-inlet-22565.herokuapp.com",
    headers: {
        'Access-Control-Allow-Origin': '*',
        "Content-type": "application/json"
    },
});