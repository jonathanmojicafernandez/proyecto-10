import axios from "axios";

var tokenStorage = localStorage.getItem('token');

export default axios.create({
    //baseURL: "http://localhost:8080/api",
    baseURL: "https://secret-inlet-22565.herokuapp.com/api",
    headers: {
        'x-access-token': tokenStorage,
        'Access-Control-Allow-Origin': '*',
        "Content-type": "application/json"
    },
});