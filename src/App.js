import React, {useReducer,useEffect} from 'react';
import Tablero from './components/tablero/Tablero';
import './App.css';
import { AuthReducer } from './reducers/AuthReducer';
import { AuthContext } from './context/AuthContext';
import LoginRouter from './route/LoginRouter';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  
} from "react-router-dom";
import Registro from './components/login/Registro';
import Login from './components/login/Login';

const init = () => {
  return JSON.parse(localStorage.getItem('log')) || { log: false }
};

function App() {
  const [log, dispatch] = useReducer(AuthReducer, {}, init);

  useEffect(() => {
    localStorage.setItem('log', JSON.stringify(log));
  }, [log])


  return (
    
    //  <Router>
    //    <Switch>
    //      <Route exact path="/">
    //        <Login   />
    //      </Route>
    //      <Route exact path="/Registrarse">
    //        <Registro   />
    //      </Route>
        
    //      <Tablero />
    //    </Switch>
    //  </Router>

    <AuthContext.Provider value={{ log, dispatch }}>
      <LoginRouter />
    </AuthContext.Provider>
     
  );
}

export default App;
