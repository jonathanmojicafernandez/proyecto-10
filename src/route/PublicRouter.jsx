import React from 'react';
import { Redirect, Route } from 'react-router';

/**
* 
* @param auth parámetro que indica si el usuario está autenticado
* @param component componente a renderizar
* @param rest el resto de los props
* @returns 
*/

// const PublicRouter = ({ component: Component }) => {
//     return <Route component={<Component />} />
// };

const PublicRouter = ({ auth, component: Component, ...rest }) => {
    // return <Route {...rest} component={() => <Component />} />
    return (
        <Route
            {...rest}
            //Protejemos la ruta verificando el valor de auth si es false cargamos el login de lo contrario redireccionamos a /
            component={(props) => !auth.log ? <Component {...props} /> : <Redirect to="/" />} />
    )
};

export default PublicRouter;