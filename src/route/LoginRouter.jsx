import React, { useContext } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import PublicRouter from './PublicRouter';
import PrivateRouter from './PrivateRouter';
import Tablero from "../components/tablero/Tablero"; 
import Login from "../components/login/Login"; 
import Registro from "../components/login/Registro"; 


const LoginRouter = () => {
    //Recuperamos el context mediante el hook useContext para así poder saber si estamos autenticados o no.
    const { log } = useContext(AuthContext);
    return (
        <Router>
            {/* <AppRouter /> */}
            {/* <Route exact path="/login" component={ LoginScreen } /> */}

            <Switch>
                <PublicRouter path="/login" auth={log} component={Login} />
                <PublicRouter path="/Registrarse" auth={log} component={Registro} />
                <PrivateRouter path="/" auth={log} component={Tablero} />
            </Switch>
        </Router>
    );

}

export default LoginRouter
